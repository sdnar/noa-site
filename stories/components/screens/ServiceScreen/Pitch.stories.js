import React from 'react';
import { storiesOf } from '@storybook/react';
import Pitch from '../../../../src/components/screens/ServiceScreen/Pitch';

import { Debug, JsonView, JsonPre } from '../../../../src/styles';

import { services } from '../../../../src/data/services.db';
import { MEDIA_SERVICES } from '../../../../src/NoaMedia'; 


export const servicesPage = {
  pick: {
	  icon: `${MEDIA_SERVICES}/software_develop_45.jpg`,
	  title: "Expertos en desarrollo de productos digitales",
	  description: "Transformamos problemas complejos en soluciones de futuro acompanando a nuestros clientes."
  }
};

const props = { ...servicesPage.pick };


storiesOf("Screens|ServiceScreen/Pitch", module)
.addParameters({ component: Pitch })
.add("default", () => (
<>
  <JsonView src={props} title="Pitch"/>
  <Pitch {...props} />
</>)
);
