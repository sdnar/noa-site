import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Header from '../../../src/components/Header';

export default {
  title: 'layout|Header',
};

export const headerDefault = () => (
	<Header>Welcome</Header>
);

