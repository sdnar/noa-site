import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Footer from '../../../src/components/Footer';

export default {
  title: 'layout|Footer'
};

const links = [
	{ label: "Nosotros", link: "/nosotros", alt: "" },
	{ label: "Servicios", link: "/servicios", alt: "" },
	{ label: "Empleo", link: "/empleo", alt: "" },
	{ label: "Contacto", link: "/contacto", alt: "" }
];

const company = { name: "Soft del Noa" };

const year = { year: "2019" };

export const footer = (props) => (
   <Footer links={links} company={company} year={year} />
);
