import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Debug, JsonView, JsonPre  } from '../../../src/styles';

export default {
  title: 'labs|Flex'
};


const styles = {
  container : {
  },
  box : {
  }
}

export const rowLayout = () => (
	<Debug style={{padding: "50px"}}>
	<div className="columns" style={styles.container}>
		<div class="column" style={styles.box}>BOX1</div>
		<div class="column" style={styles.box}>BOX1</div>
		<div class="column" style={styles.box}>BOX1</div>
	  </div>
	</Debug>
);
