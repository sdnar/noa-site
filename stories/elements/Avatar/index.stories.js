import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Avatar } from '../../../src/elements';

import Noa from '../../../src/Noa';
import NoaSite from '../../../src/NoaSite';

import { Debug, JsonView, JsonPre  } from '../../../src/styles';

export default { title: 'elements|Avatar', };

import styled from 'styled-components';


const styles = {
	MultiSliderBannerclientsList: {
	},
	clientsTitle: {
	},
};

const AvatarBox = (props) => (
	<div className="column is-2" >
			{props.children}
	</div>
) ;


export const avatar = () => {

	const clients = Object.entries(Noa.clientes).map(item => {
		return { name: item[0].name, logo: item[1].media.logos[0], css: item[1].media.logos[0].css };  });

	return (
	<div  className="has-background-light  " style={{ padding: "20px"}} >

	<div  className="columns column is-full" style={ { marginBottom: "1rem" } } >
		<h1 className="column" >TIUTLO</h1>
	</div>

	<div  className="is-full column columns is-centered" >
	<div className="is-full column columns is-multiline is-centered" style={
		{ 
			maxWidth: "66rem",
			minHeight: "120px",

			alignItems: "center",
			flexWrap: "wrap",
			justifyContent: "space-around"
		}} >
	{ clients.map((client, idx) => <AvatarBox key={idx} > <Avatar image={client.logo.picUri} name={client.name} css={client.css} /> </AvatarBox>) }


	</div>
	</div>
	</div>
	);
}

