import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ConsultoriaPage from '../../src/pages/consultoria';

import { Layout } from '../../src/global/Layout';

export default {
  title: 'pages|Consultoria',
};

export const consultoria = () => (
	<ConsultoriaPage/>

);
