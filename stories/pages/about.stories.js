import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import AboutPage from '../../src/pages/about';

export default {
  title: 'pages|About/V0',
};

export const about = () => (
	<AboutPage />
);
