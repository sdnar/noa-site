import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import IndexPage from '../../src/pages';

export default {
  title: 'pages|Home',
};

export const homeDefault = () => (
	<IndexPage />
);
