import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { ServicesPage } from '../../src/pages/services';
import { DefaultServicesPage } from '../../src/pages/services';

import { services } from '../../src/data/services.db';
import { Debug, JsonView, JsonPre } from '../../src/styles';

import { MEDIA_SERVICES } from '../../src/NoaMedia'; 

const servicesPage = {
  pick: {
	  icon: `${MEDIA_SERVICES}/software_develop_45.jpg`,
	  title: "Expertos en desarrollo de productos digitales",
	  description: "Transformamos problemas complejos en soluciones de futuro acompanando a nuestros clientes."
  },
  stats:[
	{ name: "Proyecto", count: 120, message: "Transformando el mercado digital, desde la banca digital hasta plataformas educativas." , more: 0  },	
	{ name: "Cliente", count: 20, message: "Acompanandolos con soluciones digitales en sus proyectos mas estrategicos.", more: 0   },
	{ name: "Horas", count: "+1M", message: "Un Millar de Horas desarrollando soluciones de futuro, siendo referentes en eficacia y buenas practicas.", more: 1   }
  ]
};

const props = { data: {...servicesPage}};

storiesOf("Pages|ServicesPage/Page", module)
.addParameters({component: ServicesPage })
.add('default', () => (
<>
<ServicesPage {...props} />
</>
));

storiesOf("Pages|ServicesPage/Default", module)
.addParameters({component: DefaultServicesPage })
.add('default', () => (
<>
<DefaultServicesPage />
</>
));
