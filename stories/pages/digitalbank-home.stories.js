import React from 'react';

import  { storiesOf } from '@storybook/react';
import  { action } from '@storybook/addon-actions';

import DigitalBankHome from '../../pages/digitalbank-home';


export default {
	title: 'pages|DigitalBankHome',
};

export const digitalBankHome = () => (
		<DigitalBankHome/>
);
