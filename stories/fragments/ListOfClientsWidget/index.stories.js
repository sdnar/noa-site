import React from 'react';
import styled from 'styled-components';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import  Noa  from '../../../src/Noa';
import  NoaSite from '../../../src/NoaSite';

import { css } from '@emotion/core';

import { Lab, Debug, JsonView, JsonPre } from '../../../src/styles';

import ListOfClientsWidget from '../../../src/fragments/ListOfClientsWidget';
import { AvatarsOfClients, ListOfAvatars } from '../../../src/fragments/ListOfClientsWidget';
import { SloganBanner } from '../../../src/fragments/ListOfClientsWidget';


const clientsSection = NoaSite.pages.home.sections.clientsSection;
// const clients =   clientsSection.clients.map(client => { return  {name: client.name, logo: client.media.logos[0] };});
const clients =   clientsSection.clients;
const avatars  = clients.map( client => { return { name: client.name, image: client.logo, css: client.logo.css}; });

export default { title: "fragments|ListOfClientsWidget" };



export const sloganBannerEmpty = () => (
	<Lab>
	<SloganBanner title={clientsSection.title} subtitle={clientsSection.subtitle} legend={clientsSection.legend} />
	</Lab>
);


export const sloganBannerWithContent = () => (
	<Lab>
	<SloganBanner title={clientsSection.title} subtitle={clientsSection.subtitle} legend={clientsSection.legend} >
	   <ListOfAvatars avatars={avatars} />
	</SloganBanner>
	</Lab>
);


export const listOfAvatars = () => (
   <Lab>
   <ListOfAvatars avatars={avatars} />
   </Lab>
)

export const avatarsOfClients = () => (
<Lab>
   <AvatarsOfClients clients={clients} />
</Lab>
);

export const listOfClientsWidget = () => (
<Lab>
	<ListOfClientsWidget  clients={clients} 
	title={clientsSection.title} subtitle={clientsSection.subtitle} legend={clientsSection.legend} />
</Lab>
);
