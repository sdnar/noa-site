import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import WorkingBanner from '../../../src/fragments/WorkingBanner';
import { MessageBox, CenterFormBtn } from '../../../src/fragments/WorkingBanner';

import TheTeam from '../../../src/assets/noa/images/the_team_01.jpg';

import Noa from '../../../src/Noa';
import NoaSite from '../../../src/NoaSite';

const _working = NoaSite.pages.home.sections.workingSection;

export default {
  title: 'fragments|WorkingSection',
};

export const workingSection = () => (

	<section className="hero is-fullheight is-light" >
          <WorkingBanner
              backgroundImage={_working._meta.backgroundImage}
              title={_working.title}
              subtitle={_working.subtitle}
              legend={_working.legend}
              contactBtn={_working._meta.contactBtn}
          />
	</section>
);

export const messageBox = () => (
	<div className="hero-body  p-b-30 has-background-info" >
	<MessageBox 
        title={_working.title}
        subtitle={_working.subtitle}
        legend={_working.legend}
	contactBtn={_working._meta.contactBtn} />
	</div>
);

export const centerFormBtn = () => (
	<CenterFormBtn>{_working._meta.contactBtn}</CenterFormBtn>
);
