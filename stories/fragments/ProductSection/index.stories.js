import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ProductSection from '../../../src/fragments/ProductSection';
import { ProductCard,
	 ProductsSlider,
	ProductMainBox,
	ProductsContainer } from '../../../src/fragments/ProductSection';



import Noa from '../../../src/Noa';
import NoaSite from '../../../src/NoaSite';

const _products = NoaSite.pages.home.sections.productsSection;

export default {
  title: 'fragments|ProductSection',
};


export const productSection  = () => (

	<section className="hero is-fullheight is-light" >

	     <ProductSection
		     title={_products.title}
		     subtitle={_products.subtitle}
		     legend={_products.legend}
		     mainProduct={_products.mainProduct}
		     otherProducts={_products.otherProducts}
	     />

	</section>
);

export const productsContainer = () => (
	<div className="has-background-light " style={{ width: "100%", height: "30%", padding: "5%"}} >
		<ProductsContainer mainProduct={_products.mainProduct} otherProducts={_products.otherProducts} />
	</div>
);

export const productMainBox = () => (
	<div className="has-background-light" style={{ width: "100%", height: "30%", padding: "5%"}} >
		<ProductMainBox  product={_products.mainProduct}/>
	</div>
);

export const productsSlider = () => (
	<div   className="has-background-light" style={{ width: "100%", height: "30%", padding: "5%"}} >
	<ProductsSlider products={_products.otherProducts} />
	</div>
);

export const productCard = () => (
	<div  className="has-background-light" style={{ width: "50%", height: "30%", padding: "5%"}} >
		<ProductCard product={_products.otherProducts[0]}/>
	</div>
);
