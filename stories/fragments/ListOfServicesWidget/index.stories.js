import React from 'react';
import styled from 'styled-components';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Noa, NoaSite } from '../../../src';

import { Lab, Debug, JsonView, JsonPre } from '../../../src/styles';

import ListOfServicesWidget from '../../../src/fragments/ListOfClientsWidget';
import { ListWidget, ListOfItems, Item} from '../../../src/fragments/ListOfServicesWidget';
import { SloganBanner } from '../../../src/fragments/ListOfServicesWidget';



