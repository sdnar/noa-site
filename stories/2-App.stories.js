import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

export default {
  title: 'tmp|App',
};

export const appDefault = () => (
	<div />
);

export const appWithUsername = () => (
	<div>Hola</div>
);
