import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Layout from '../../../src/global/Layout';

export default {
  title: 'layout|MainLayout',
};

export const homeDefault = () => (
	<Layout>Welcome</Layout>
);

