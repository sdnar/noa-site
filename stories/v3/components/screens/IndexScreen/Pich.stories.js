import React from 'react';
import { storiesOf } from '@storybook/react';
import Pitch from '../../../../../src/v3/components/screens/IndexScreen/Pitch';

import db from '../../../../../src/data/digitalbank.db';

const props = {
			title: db.sections.digitalExperience.title,
			subtitle: db.sections.digitalExperience.subtitle 
}

storiesOf('Screens|IndexScreen/Pitch', module)
  .addParameters({ component: Pitch })
  .add('default', () => <Pitch {...props} />);
