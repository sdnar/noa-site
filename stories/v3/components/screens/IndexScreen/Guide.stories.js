import React from 'react';
import { storiesOf } from '@storybook/react';
import Guide from '../../../../../src/v3/components/screens/IndexScreen/Guide';
import db from '../../../../../src/data/digitalbank.db';

/** #6f2CAC **/
const interbankingCase = db.sections.digitalExperience.projects[0];

/* "Conector online/ofline para los servicios de Banca Electronica Multibanco.", */
/* `${MEDIA_DIGITAL_BANKING}/logo_bancopiano.png`, */
/* 'Integracion con Interbanking DataNet', */

const props = {
		chapterCount: `${interbankingCase.client}`,
  themeColor: 'linear-gradient(340deg, rgba(111,44,172,0.35), rgba(11,44,172,0.85))',
  title: interbankingCase.title, 
  description: interbankingCase.overview,
  imagePath: interbankingCase.image 
};


storiesOf('Screens|IndexScreen/Guide', module)
  .addParameters({ component: Guide })
  .add('default', () => <Guide {...props} />);
