import React from 'react';
import { storiesOf } from '@storybook/react';
import Guides from '../../../../../src/v3/components/screens/IndexScreen/Guides';
import db from '../../../../../src/data/digitalbank.db';

const nodes = db.sections.digitalExperience.projects.map((project) => {
			return {
						node: {
						  fields: {
								   guide: '#',
								   slug: '#'
								},
								frontmatter: {
											description: project.overview,
											title: project.title,
											themeColor: project.themeColor,
											thumbImagePath: project.image
								}
						}
			};
}
);

const props = {
  chaptersEdges: [
    {
      node: {
        fields: {
          guide: 'guide',
        },
      },
    },
  ],
  guidesEdges: nodes,
};

storiesOf('Screens|IndexScreen/Guides', module)
  .addParameters({ component: Guides })
  .add('default', () => <Guides {...props} />);

