import React from 'react'
import { storiesOf } from '@storybook/react';

import { JsonView } from '../../../../../src/styles';

import ContactForm from '../../../../../src/v3/components/screens/ContactScreen/ContactForm';
import ContactScreen from '../../../../../src/v3/components/screens/ContactScreen';
import db from '../../../../../src/data/general.db';

import styled from 'styled-components';

import { contactForm, contactPage  } from '../../../../../src/data/contact.db';

const SandBox = styled.div`
  margin: 100px;
  background: #f6f9fc;
`;

const props = {
   ...contactPage
};

storiesOf('Screens|ContactScreen/ContactForm', module)
.addParameters({ component: ContactForm })
.add('default', () => 
   <SandBox>
   <ContactForm {...contactForm}/>
   </SandBox>
);

storiesOf('Screens|ContactScreen/ContactScreen', module)
.addParameters({ component: ContactScreen })
.add('default', () => 
   <SandBox>
   <ContactScreen {...contactPage}/>
   </SandBox>
)
