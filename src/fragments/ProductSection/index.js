import React from 'react';
import styled from 'styled-components';

import { css } from '@emotion/core';

import { Debug, JsonView, JsonPre  } from '../../styles';

import Layout from '../../global/Layout';
import Image from '../../components/Image';

const Title = (props) => (
   <h1 className="title"
	  dangerouslySetInnerHTML={{__html: props.children}} />
);

const Subtitle = (props) => (
  <h2 className="subtitle" 
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


const Legend = (props) => (
  <h3 className="subtitle"
	  dangerouslySetInnerHTML={{__html: props.children}} />
);

export const ProductsContainer = (props) => (
	<div className="container"  >
	  <ProductMainBox  className="column" product={props.mainProduct}/>
     	  <div class="is-divider" data-content="- " style={{margin: "2%", borderTop: ".1rem solid #dbdbdb"}} />
	  <ProductsSlider className="column" products={props.otherProducts}/>
	</div>
);

export const ProductMainBox = (props) => (
	<div class="box">      
		<ProductMainTube className="box" video={props.product.promos.videos[0]}/>
	</div>
);

const  ProductMainTube = (props) => (
	<iframe 
		style={{ width: "730px", height: "411px"}}
		className="m-t-50" src={ `https://www.youtube.com/embed/${props.video.tubeKey}` }
		frameBorder="0"
		allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
		allowFullScreen>
	</iframe>
);

export const ProductsSlider = (props) => (
	<div className="columns">
		<ProductSliderItem  className="column is-6" product={props.products[0]} />
		<ProductSliderItem  className="column is-6" product={props.products[1]} />
	</div>
);

export const ProductCard = (props) => (
   <div className="columns">
	<ProductCardHeader className="column" product={props.product}/>
	<ProductCardBody className="column" product={props.product}/>
   </div>
);

const ProductCardHeader = (props) => (
   <div className="column">
   <h1 className="title m-t-60"
	  dangerouslySetInnerHTML={{__html: props.product.name}} />
   <h2 className="subtitle"
	  dangerouslySetInnerHTML={{__html: props.product.summary}} />
   </div>
);

const ProductCardBody = (props) => (
  <div className="column">
   <img src={props.product.promos.images[0].picUri} alt={props.product.summary} />
   </div>
);

const ProductSliderItem = (props) => (
	<>
	<ProductCard {...props} /> 
	</>
);


const ProducstSection = (props) => (
    <div className="hero-body is-medium has-text-centered ">
	<div className="container">
	  <div className="column">
  	     <Subtitle>{props.subtitle}</Subtitle>
	     <Title>{props.title}</Title>
  	  </div>
	  <ProductsContainer className="column" mainProduct={props.mainProduct} otherProducts={props.otherProducts}/>
	  <Legend>{props.legend}</Legend>
       </div>
    </div>
);

export default ProducstSection;
