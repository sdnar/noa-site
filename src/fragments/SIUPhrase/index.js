import React from 'react';
import { useEffect, useState } from 'react';
import { useRef } from 'react'

import styled from 'styled-components';

import { css } from '@emotion/core';

import { LayoutLite } from '../../global/Layout';

import {Line, Divider, Debug  } from '../../styles';

import Hero from '../../components/Hero';
import { NoHero } from '../../components/Hero';
import HeroCarousel from '../../components/HeroCarousel';


const SiuPhrase = ( { phrase } ) => {

const styles = {
  widgetText: {
	  textAlign: "center",
	  margin: "0 5%",
  }
}

return (
<Hero  backgroundImage={phrase.backgroundImage} 
	css={{...phrase.css.backgroundStyle}} >
	<aside style={styles.widgetText}>
		<p style={{...phrase.css.phraseStyle}} 
		  dangerouslySetInnerHTML={{__html: phrase.phrase}} />
	</aside>
</Hero>);

};

export default SiuPhrase;
