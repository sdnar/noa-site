import React from 'react';

import styled from 'styled-components';


import { Debug, JsonView, JsonPre } from '../../styles';
import { Noa, NoaSite } from '../../';

const styles = { 
  ListContainer: {  }
};

export const CenterFormBtn = (props) => {

return (
  <form>
    <div className="field ">
      <div className="control is-expanded has-text-centered has-shadow-field">
         <button type="button" className="button is-large is-success is-centered is-expanded" style={{...styles.actionButton, ...props.css.actionStyle}}>
           <span className="has-text-weight-semibold is-expanded">{props.label}</span>
         </button>
      </div>
    </div>
  </form>
);
}



const Title = (props) => (
  <h1 className="title is-size-2 has-text-centered" style={ {color: "white", fontWeight: "600"}}
	  dangerouslySetInnerHTML={{__html: props.children}} />
);

const Subtitle = (props) => (
  <h2 className="subtitle cta" style={{ color: "white", fontWeight: "400" }} 
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


const Legend = (props) => (
  <h3 className="subtitle is-size-10 cta" style={{ color: "white", fontWeight: "400" }}
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


export const SloganBanner = (props) => {

const styles = {
  heroContainer: {
	backgroundColor: "#b5b5b5"
  },
  actionButton: {
  }
}

/* <ActionButton /> */
return  ( <>
	  <div class="is-height is-fullwidth hero" style={{...styles.heroContainer, ...props.css.heroContainer, width: "100%"}} >
		  <div style={{ margin: "25px" }} >
	     <Legend>{props.legend}</Legend>
	     <Title>{props.title}</Title>
  	     <Subtitle>{props.subtitle}</Subtitle>
		{props.children}
	     </div>
  	  </div>
  </> );
};

export const WeHelpYouWidget = (props) => (
	<div class="column hero is-height is-fullwidth" style={{ width: "100%" }}> 

  <SloganBanner title={props.title} subtitle={props.subtitle} legend={props.legend} css={props.css}  >
  <CenterFormBtn  onSubmit={props.accBtn.handler} label={props.accBtn.label} css={props.css} />
  </SloganBanner>
  </div>
);

export default WeHelpYouWidget;
