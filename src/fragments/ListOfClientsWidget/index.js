import React from 'react';
import styled from 'styled-components';

import { css } from '@emotion/core';

import { Debug, JsonView, JsonPre } from '../../styles';


import { Avatar } from '../../elements/Avatar';

const Title = (props) => (
   <h1 className="title is-size-4"
	  dangerouslySetInnerHTML={{__html: props.children}} />
);

const Subtitle = (props) => (
  <h2 className="subtitle is-size-6" 
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


const Legend = (props) => (
  <h3 className="subtitle is-size-10"
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


export const SloganBanner = (props) => (
  <>
	  <div >
	     <Title>{props.title}</Title>
  	     <Subtitle>{props.subtitle}</Subtitle>
		{props.children}
	     <Legend>{props.legend}</Legend>
  	  </div>
  </>
);

const AvatarBox = (props) => (
   <div className="column is-2" >
   {props.children}
   </div>
) ;

const styles = {
   ListContainer: {  minHeight: "120px", alignItems: "center", flexWrap: "wrap", justifyContent: "space-around" }
};

export const ListOfAvatars = (props) =>  {
  

   let avatars; 
   
   if(props.avatars) { 
        avatars =  props.avatars.map((avatar, idx) => (
     	<AvatarBox key={idx} >
 		<Avatar image={avatar.image.picUri} name={avatar.name} css={avatar.css} />
     	</AvatarBox>));
   }

   return (
   <div  className="is-full column columns is-centered" >
   <div className="is-full column columns is-multiline is-centered" style={styles.ListContainer} >
     { avatars }
     { props.children }
   </div>
   </div>
   );
}


export const AvatarsOfClients = (props) =>  {
	console.log(props);
	const avatars  = props.clients.map( client => { return { name: client.name, image: client.logo, css: client.logo.css}; });
	return ( <ListOfAvatars avatars={ avatars } /> );
}

export const ListOfClientsWidget = (props) => (
   <SloganBanner title={props.title} subtitle={props.subtitle} legend={props.legend} >
	   <AvatarsOfClients clients={props.clients} />
   </SloganBanner>
);

export default ListOfClientsWidget ;
