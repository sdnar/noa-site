import React from 'react';
import styled from 'styled-components';


import ReactJson from 'react-json-view';

import { css } from '@emotion/core';

import { JsonView, Debug, JsonPre } from '../../styles';

import Layout from '../../global/Layout';

import NoaSite from '../../NoaSite';

const Title = (props) => (
   <h1 className="title" style={{ color: "white" }}>
      <span className="is-size-2 has-text-centered is-block" style={{ color: "white"}} >
        {props.children}
      </span>
  </h1>
);

const Subtitle = (props) => (
  <h2 className="subtitle" style={{ color: "white"}} >
      <span className="has-text-centered is-block" style= {{ color: "white" }} >
        {props.children}
      </span>
  </h2>
);


const Legend = (props) => (
  <h3 className="subtitle " style={{color: "white"}}>
      <span className="has-text-centered is-block" style={{ color: "white"}}>
	{props.children}
      </span>
  </h3>
);

export const CenterFormBtn = (props) => (
<form>
  <div className="field ">
    <div className="control is-expanded has-text-centered has-shadow-field">
       <button type="button" className="button is-large is-success is-rounded is-centered is-expanded">
         <span className="has-text-weight-semibold is-expanded">{props.children}</span>
       </button>
    </div>
  </div>
</form>
);

export const MessageBox = (props) => (
 <div className="header-home__message container" style={{ color: "white" }}>
	<Legend>{props.legend}</Legend>
	<Title>{props.title}</Title>
	<Subtitle>{props.subtitle}</Subtitle>

        <div className="columns is-centered has-text-centered">
	
        <div className="column is-7">
	   <div className="">
	   <CenterFormBtn>{props.contactBtn}</CenterFormBtn>
	   </div>
	</div>
	</div>

</div>
);


const WorkingBanner = (props) => {
    return (
    <div className="hero-body  p-b-30 "
	    style={{
		    background: `linear-gradient(to top, rgba(80, 68, 18, 0.65) 25%, transparent), url(${props.backgroundImage.picUri}) no-repeat center center`, 
		    backgroundSize: "cover",
		    padding: "25%"
	    }}>
    <MessageBox  {...props} /> 
     </div>
   );
}

export default WorkingBanner;
