import React from 'react';
import styled from 'styled-components';


import { Debug, JsonView, JsonPre } from '../../styles';
import { Noa, NoaSite } from '../../';

const styles = {
   ListContainer: { maxWidth: "85rem", minHeight: "120px", alignItems: "center", flexWrap: "wrap", justifyContent: "space-around" }
};




const Title = (props) => {
let klasses = ["title", "is-size-4", "cta"];
if (props.css.fontSize) {
   klasses.pop(1);
}
return  (
   <h1 className={klasses.join(" ")} style={{textAlign: "center", ...props.css}}
	  dangerouslySetInnerHTML={{__html: props.children}} />
);
}

const Subtitle = (props) => {
let klasses = ["subtitle", "is-size-6", "cta"];
if (props.css.fontSize) {
   klasses.pop(1);
}

return  (
  <h2 className={klasses.join(" ")}  style={{...props.css}}
	  dangerouslySetInnerHTML={{__html: props.children}} />
);

}


const Legend = (props) => (
  <h3 className="subtitle is-size-10" style={{...props.css}}
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


export const SloganBanner = (props) => {

let owncss = {sloganStyle: {}, titleStyle: {}, subtitleStyle: {}, legendStyle: {}, ...props.css};
return (
  <>
	  <div style={owncss.sloganStyle}  >

	     <Title css={owncss.titleStyle} >{props.title}</Title>
  	     <Subtitle css={owncss.subtitleStyle} >{props.subtitle}</Subtitle>
		{props.children}
	     <Legend css={owncss.legendStyle} >{props.legend}</Legend>
  	  </div>
  </>
);
}


/***  LIST BANNER ****/

export const ItemSubtitle = (props) => (
		<h2 className="subtitle" style={{ fontSize: "1rem",    fontWeight: "400"}}>{props.children} </h2>
)

export const ItemTitle = (props) => (
		<h2 className="title" style={{ fontSize: "1rem",   fontWeight: "600", color: "#3273dc" }}> {props.children} </h2>
);

/* <ItemHeader> </ItemHeader> */

export const Item  = (props) => {


return (
   <div className="" >
  	   <figure  > <img src={props.image} alt={props.name} style={props.css}/> </figure>
	    <ItemTitle>{props.title}</ItemTitle>
	    <ItemSubtitle>{props.subtitle}</ItemSubtitle>
   </div>

);
}

export const ItemBox  = (props) => {
let ownclasses = ["column is-one-third vcentered "];
if(props.css && props.css.itemBoxClasses)  {
   ownclasses = [...ownclasses, ...props.css.itemBoxClasses];
}
return (
   	<div className={ownclasses.join(" ")}>
	  {props.children}
	</div>
);
}

export const ListOfItems = (props) => {
  let owncss = {listContainerStyle: {}, ...props.css};

  let items;
  if( props.items ) {
        items =  props.items.map((item, idx) => (
     	<ItemBox key={idx} css={props.css} >
		<Item 
		      title={item.title}
		      subtitle={item.subtitle} 
		      image={item.image.picUri}
		      css={item.css} />
     	</ItemBox>));
  }


  return (
   	<div  className="is-full column columns is-centered " >
        <div className="is-full column columns is-multiline is-centered is-vcentered" style={{...styles.ListContainer, ...owncss.listContainerStyle}} >
        { items }
        { props.children }
        </div>
        </div>
  );
}

export const ListWidget = (props) => (
   <SloganBanner title={props.title} subtitle={props.subtitle} legend={props.legend}  css={props.css} >

	   <ListOfItems items={props.items} css={props.css} />
   </SloganBanner>
);


/**** List of Services ***/

export  const ListOfServicesWidget = (props) => (
	<>

	<ListWidget 
		title={props.title} subtitle={props.subtitle} legend={props.legend} css={props.css}
		items={ props.services.map(service => {
			return { title: service.name, subtitle: service.overview, image: service.media.logos[0] };
		})} >

	</ListWidget>
	</>

);

export default ListOfServicesWidget;
