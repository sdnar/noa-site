import React from 'react';
import { useEffect, useState } from 'react';
import { useRef } from 'react'

import styled from 'styled-components';

import { css } from '@emotion/core';

import { LayoutLite } from '../../global/Layout';

import {Line, Divider, Debug  } from '../../styles';

import Hero from '../../components/Hero';
import { NoHero } from '../../components/Hero';
import HeroCarousel from '../../components/HeroCarousel';

export const WeAreCarousel = ( props ) => {
    return (<HeroCarousel  slides={props.slides} />);
};

export default WeAreCarousel;

/***
  <Hero  backgroundImage={_wheAreSection.slides[0].backgroundImage}
  title={_wheAreSection.slides[0].title} 
  subtitle={_wheAreSection.slides[0].subtitle}
  css={_wheAreSection.slides[0].css}
 /> */

