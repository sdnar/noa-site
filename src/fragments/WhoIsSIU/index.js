import React from 'react';
import { useEffect, useState } from 'react';
import { useRef } from 'react'

import styled from 'styled-components';

import { css } from '@emotion/core';

import { LayoutLite } from '../../global/Layout';

import {Line, Divider, Debug  } from '../../styles';

import Hero from '../../components/Hero';
import { NoHero } from '../../components/Hero';
import HeroCarousel from '../../components/HeroCarousel';

import ListOfServicesWidget  from '../ListOfServicesWidget';
import ListOfTecnologiesWidget  from '../TecnologiesList';


export const MainItemSubtitle = (props) => (
   <h2 className="subtitle" style={{ color: "#fff",  fontSize: "1.2rem", fontWeight: "400"}}>{props.children} </h2>
)

export const MainItemTitle = (props) => (
   <h2 className="title" style={{ fontSize: "32px", color: "#fff",  lineHeight: "1.6", textShadow: "1px 1px #82a4dd", fontWeight: "400"}}>{props.children} </h2>
);

/* <ItemHeader> </ItemHeader> */

/* background: "rgba(20,57,182,0.86)", */

export const MainItem  = (props) => {
return (
   <div className="" style={{ clear: "both", position: "relative", textAlign: "center" }}>
	   <div class="image-wrap">
		   <figure className=""> <img src={props.image} alt={props.name}

			   style={{height: "100%", ...props.css}}/>
	           </figure>
	   </div>
	   <div class="titledesc-wrap" style={{
			textAlign: "center",
		   background: "linear-gradient(15deg, rgba(0,184,156,0.75),  rgba(0,184,156,0.95), rgba(0,184,156,0.65), rgba(0,184,156,0.5))",
		   position: "absolute",
		   top: "0", left: "0", height: "100%", margin: "auto", right: "0",
		   zIndex: "9999"
	   }}>
	      <div style={{position: "absolute", top: "60%", padding: "15px", textAlign: "center", verticalAlign: "center"}}>
	              <div style={{marginTop: "0", maringBottom: "0", fontSize: "22px" }}>
	    	           <MainItemTitle className="" css={{fontSize: "22px" , color: "#fff"}} >{props.title}</MainItemTitle>
	              </div>
	              <div>
			   <MainItemSubtitle className="">{props.subtitle}</MainItemSubtitle>
		      </div>
	      </div>
	   </div>
   </div>

);
}

export const MainItemBox  = (props) => {
let ownclasses = ["column columns is-third  is-centered vcentered "];
if(props.css && props.css.mainBoxClasses)  {
   ownclasses = [...ownclasses, ...props.css.itemBoxClasses];
}
return (
   	<div className={ownclasses.join(" ")} style={{}} >
	  {props.children}
	</div>
);
}


export const WhoIsSIU = (props) => {

let main = props.products[0];
let others = [props.products[1], props.products[2], props.products[3]];
console.log(others);

return (<>
<NoHero  title={props.title}
	subtitle={props.subtitle}
	classes=""
	css={{...props.css}} >

  	<MainItemBox css={props.css}>
	  <MainItem 
		  item={main}
		  css={props.css.mainItemStyle}
		  title={main.title}
		  subtitle={main.overview}
		  image={main.media.logos[0].picUri}
	  />
   </MainItemBox>
</NoHero>
<Line/>
     <ListOfTecnologiesWidget 
	     title={props.othertitle}
	     subtitle=""
	     legend=""
	     items={others}
	     css={props.css}
     /> 

</>);
}

export default WhoIsSIU;

/*
    <WhoIsSIU  css={_whoIsSIU.css}
     title={_whoIsSIU.title}
     subtitle={_whoIsSIU.subtitle}
     othertitle={_whoIsSIU.othertitle}
     products={_whoIsSIU.products} />
*/

