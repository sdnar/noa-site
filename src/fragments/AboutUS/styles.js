export let styles = {
bdLinks : {
	 display: "flex",
	 flexWrap: "wrap",
	 counterReset: "bd-links",
	 boxSizing: "inherit"
},

bdLink : {
	 width: "100%",
	 borderRadius: "6px",
	 display: "block",
	 fontSize: "1.25rem",
	 padding: "1rem 3rem 1.5rem 5rem",
	 position: "relative",
	 transitionDuration: "86ms",
	 transitionProperty: "background-color,color"
},

bdLinkName : {
	color: "#363636",
	fontSize: "1.5rem",
	fontWeight: "600",
	lineHeight: "1.25",
	padding: "0",
	margin: "0",
	marginBottom: ".25rem",
	position: "relative"
},

bdLinkFigure : {
	position: "absolute",
	right: "calc(100% + .75em)",
	textAlign: "center",
	top: "0",
	minWidth: "1.5em"
},

bgLinkIcon: {
	display: "block",
	fontSize: "2rem",
	width: "1.5em",
	color: "#3273dc",
	height: "100%"
},

bgLinkSubtitle: {
	display: "block",
	margin: "0",
	padding: "0",
	color: "#7a7a7a",
	fontSize: "1.25rem"
}


}



export default styles;
