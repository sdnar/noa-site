import React from 'react';

import { Line, Divider, Debug, JsonView, JsonPre } from '../../styles';

import Hero from '../../components/Hero';
// import styles from './styles.css';
import styles from './styles';

const Item = ({item}) => (
	<>
	<a style={styles.bdLink} href="#" >
	  <h2 style={styles.bdLinkName}>
	    <figure style={styles.bdLinkFigure}> 
		<span className="icon bg-link-icon has-text-link" style={styles.bgLinkIcon}>
		   <i class={item.icon}></i>
		</span>
	    </figure> {item.name}</h2>	
	  <p class="bg-link-subtitle" style={styles.bgLinkSubtitle}>{item.overview}</p>
       </a>
       </>
);

const ListOfItems = (props) => {

let items = props.items.map(item => (
  <Item item={item}/>
));

return(
<div style={props.css.ListBoxStyle} className={styles.bdLinks} >
{items}
</div>
);
};


export const AboutUSWidget = (props) => {

return (<>
<Hero  title={props.title}
	subtitle={props.subtitle}
	classes=""
	css={props.css} >

	<div className="columns">
	<div className="column" css={props.css}>

  	<h1 className="title is-size-3 has-text-centered" style={ {color: "#363636", fontWeight: "400", fontSize: "32px"}}
	  dangerouslySetInnerHTML={{__html: props.title}} />

	<aside style={styles.overview} >
	<p  style={ { 
		 width: "85%",
		margin: "0px auto 40px",
		color: "rgb(122,122,122)",
		textAlign: "left",
		fontSize: "1.25rem",
		fontWeight: "400"
	}} 
	dangerouslySetInnerHTML={{__html: props.overview}} />

	</aside>
	<aside style={styles.overview} >
	<p  style={ { 
		 width: "85%",
		margin: "0px auto 40px",
		color: "rgb(122,122,122)",
		textAlign: "right",
		fontSize: "1.25rem",
		fontWeight: "400",
		fontStyle: "italic"
	}} 
	dangerouslySetInnerHTML={{__html: props.owner}} />
	</aside>



   	</div>
  	<div className="column" css={props.css}>
	  <ListOfItems 
		  items={props.services}
		  css={props.css}
		  title=""
		  subtitle=""
		  image=""
	  />
   	</div>
	</div>
</Hero>

</>);
}



export default AboutUSWidget;
