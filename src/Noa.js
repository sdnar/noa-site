import logo from './assets/noa/images/logo.png';

import NoaMedia from './NoaMedia';

const dflow = {
  titulo: "DFLOW", 
  name: "DFLOW",
  descripcion: "Solucion de Gestion Online para pequenas empresas.",
  summary: "Solucion de Gestion Online para pequenas empresas.",
  overview: "Solucion de Gestion Online para pequenas empresas.",
  site: "http://dflow.sofdelnoa.com.ar",
  landing: "http://softdelnoa.com.ar/products/dflow",
  promos: {
    images: NoaMedia.company.products.dflow.images,  
    videos: NoaMedia.company.products.dflow.videos,
  }
};


const durban = {
  titulo: "DURBAN", 
  name: "DURBAN",
  descripcion: "Plataforma onlin de facturacion electronica.",
  summary: "Plataforma online de facturacion electronica.",
  overview: "Plataforma online de facturacion electronica.",
  site: "http://durban.sofdelnoa.com.ar",
  landing: "http://softdelnoa.com.ar/products/durban",
  promos: {
    images: NoaMedia.company.products.durban.images,  
    videos: NoaMedia.company.products.durban.videos,
  }
};

const soporte   = {name: "Soporte a desarrollo", overview: "Construimos entornos solidos para los proyectos." 
	, overviewFull: "Construimos entornos solidos para los proyectos, trabajando a diario con el equipo de desarrollo." };
const mobile    = {name: "Desarrollo Mobile", overview: "Convertimos tus ideas en productos de valor."
	, overviewFull: "Convertimos tus ideas en productos de valor para los usuarios finales." };
const disenio   = {name: "Sofware a medida", overview: "Especializados en aplicaciones web y plataformas en la nube."
	, overviewFull: "Estamos especializados en aplicaciones web y desarrollos de plataformas en la nube." };
const agile     = {name: "Acompanamiento Agile", overview: "Implantamos modelos agiles en las organizaciones." 
, overviewFull: "Implantamos, acompanamos y ayudamos a escalar modelos agiles en las organizaciones." };
const software  = {name: "Sofware a medida", overview: "Especializados en aplicaciones web y plataformas en la nube."
, overviewFull: "Estamos especializados en aplicaciones web y desarrollos de plataformas en la nube." };
const auditoria = {name: "Auditoria en Seguridad y Desarrollo" , overview: "Analizamos la calidad tecnica de tus productos."
, overviewFull: "Analizamos la calidad tecnica de tu productos y te ayudamos a ganar productividad." };
const consultoria = {
	name: "Consultoria Especializada en SIU", 
	overview: "Desde la implementacion a realizar formacion.",
	overviewFull: "Desde la implementacion del Sistema Educativo a realizar formacion especifica.",
	services: [
	 { name: "Consultoria SIU",
	   overview: "Asesoramiento y Consultoria SIU de Alto Nivel"
	 },
	 { name: "Cursos SIU",
	   overview: "Disenados para adquirir excelencia en SUI y sus principales modulos."
	 },
	{ name: "SIU para su Institucion",
	   overview: "Evaluamos, Implementamos, Recuperamos y Optimizamos SIU."
	 },
	{ name: "Gestion de Talento",
	   overview: "Encontramos los Profesionales SIU mas cualificados para tu Institucion."
	 }
	]
};

const MediaClients = NoaMedia.company.clients;


const Noa = {
  owner: "Ing. Sergio Paz",
  logo: NoaMedia.company._logos[0],
  name: "SOFT DEL NOA",
  overview: " Empresa desarrollo IT, mobile, software | Soft del Noa ",
  summary: " Soft del Noa: Empresa desarrollo IT que brinda servicios tecnologicos desarrollo, operacion y consultoria de software y servicio informaticos.  ",
  products: { dflow, durban },
  clientes: {
	  arsat:         { name: "Arsat ", overview: "", media: MediaClients.arsat, projectos: [] },
	  modernizacion: { name: "Ministerio de Modernizacion", overview: "", media: MediaClients.modernizacion, projectos: [] },
	  piano:         { name: "Banco Piano", overview: "", media: MediaClients.piano, projectos: [] },
	  siu:           { name: "SIU - Sistema de Informacion Universitaria", overview: "", media: MediaClients.siu, projectos: [] },
	  caba:          { name: "Ciuda Autonoma de Buenos Aires", overview: "", media: MediaClients.caba, projectos: [] },
	  una:           { name: "Universidad Nacional de las Artes", overview: "", media: MediaClients.una, projectos: [] },
	  fiuba:         { name: "FIUBA - Facultad de Ingenieria - UBA", overview: "", media: MediaClients.fiuba, projectos: [] },
	  fadu:          { name: "FADU - Facultad de Arquitectura, Disenio y Urbanismo", overview: "", media: MediaClients.fadu, projectos: [] },
	  pna:           { name: "Facultad de la Policia Nacional Argentina", overview: "", media: MediaClients.pna, projectos: [] },
	  inun:          { name: "INUN - Instituto Universitario Nava - Armada Argentina", overview: "", media: MediaClients.inun, projectos: [] },
	  hibu:          { name: "HIBU ", overview: "", media: MediaClients.hibu, projectos: [] },
	  guru:          { name: "GURU ", overview: "", media: MediaClients.guru, projectos: [] },
	  unla:          { name: "UNLA - Universidad de Lanus", overview: "", media: MediaClients.unla, projectos: [] },
	  unsam:         { name: "UNSAM - Universidad Nacional de San Martin", overview: "", media: MediaClients.unsam, projectos: [] },
	  pfa:           { name: "Facultad de la Policia Federal Argentina", overview: "", media: MediaClients.pfa, projectos: [] },
	  undec:         { name: "UNDEC - Universidad Nacional de Chilecito", overview: "", media: MediaClients.undec, projectos: [] },
  },
  services: { soporte, mobile, disenio, agile, software, auditoria, consultoria },
  siteUrl: "www.softdelnoa.com.ar"
}

export default Noa;
