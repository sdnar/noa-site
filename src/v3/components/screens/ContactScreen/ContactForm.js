import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import pluralize from 'pluralize';
import { styles, Icon, Subheading } from '@storybook/design-system';

import settings from '../../../../NoaSettings';

import "../../../../assets/css/forms.css";

const { breakpoint, color, spacing, typography } = styles;

const ContactFormWrapper = styled.div`
  margin-top: 50px;
  display: block;
  background: ${props => props.themeColor};
  color: ${color.lightest};
  padding: 40px;
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  text-align: left;
  height: 400px;

  @media (min-width: ${breakpoint}px) {
    height: 450px;
  }

  @media (min-width: ${breakpoint * 1.25}px) {
    height: 400px;
  }
`;

const NoDisplay = styled.div`
  display: none;
`;

const InputsWrapper = styled.div`
`;

const TwoColumn = styled.div`
  background: orage;
	display: flex;
  flex-wrap: wrap;
  justify-content: center;
	flex-direction: row;
`;

const Column = styled.div`
  background: orage;
	width: 50%;
	padding-left: 10px;
	padding-right: 10px;
`;


const InputWrap = styled.div`
  margin-bottom: 30px;
`;

const RelativeSpan = styled.span`
  position: relative;
  line-height: 1;
`;

const SendButton = styled.button`
  padding: 10.5px 40px;
	background-color: transparent;
	border: 2px solid #10069F;
	color: #10069F;
	font-family: "Work Sans", sans-serif;
	font-size: 18px;
	font-weight: 500;
	text-transform: uppercase;
	transition: .3s;
	border-radius: 0;
`;

const handleSubmit = (event) => {
				event.preventDefault();
				const data = new FormData(event.target);
				fetch(settings.backend.api["contact-send"], {
					method: 'POST',
					body: data,
				});
				fetch(settings.backend.api["contact-save"], {
					method: 'POST',
					body: data,
				});
}


export const ContactForm = ({origin, locale, form}) => {

  return(
     <div class="au">
     <ContactFormWrapper themeColor="#fff" >
        <div class="contact-form">
	     <p>{form.legend}</p>
	     <div>
		     <form onSubmit={handleSubmit} novalidate="novalidate">
		         <NoDisplay>
				 <input type="hidden" name="Origin" value={origin}/>
				 <input type="hidden" name="Locale" value={locale}/>
			 </NoDisplay>
			 <InputsWrapper>
				<TwoColumn>
								<Column>
				<InputWrap>
					<RelativeSpan>
						<input type="text" name="ContactName" size="40"
						placeholder={form.fields.ContactName.label}/>
					</RelativeSpan>
				</InputWrap>
								</Column>
								<Column>
				<InputWrap>
					<RelativeSpan>
						<input type="text" name="ContactEmail" size="40"
						placeholder={form.fields.ContactEmail.label}/>
					</RelativeSpan>
				</InputWrap>
								</Column>
				</TwoColumn>

				<TwoColumn>
								<Column>
				<InputWrap>
					<RelativeSpan>
						<input type="text" name="ContactPhone" size="40"
						placeholder={form.fields.ContactPhone.label}/>
					</RelativeSpan>
				</InputWrap>
								</Column>

								<Column>
				<InputWrap>
					<RelativeSpan>
						<input type="text" name="ContactCompany" size="40"
						placeholder={form.fields.ContactCompany.label}/>
					</RelativeSpan>
				</InputWrap>
								</Column>
				</TwoColumn>

				<InputWrap>
					<label for="Message">{form.fields.Message.label}</label>
					<RelativeSpan>
						<textarea type="text" name="Message" cols="40" rows="10" />
					</RelativeSpan>
				</InputWrap>
				<InputWrap>
					<RelativeSpan>
					  <SendButton>{form.fields.Send.label} </SendButton>
					</RelativeSpan>
				</InputWrap>

			 </InputsWrapper>
			 
		     </form>
	     </div>
	</div>
     </ContactFormWrapper>
     </div>
  );
}

export default ContactForm;
