import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


import WeHelpYouWidget from '../../../../fragments/WeHelpYouWidget';
import ListOfServicesWidget from '../../../../fragments/ListOfServicesWidget';

import { Debug, JsonView, JsonPre  } from '../../../../styles';
import { Line, Divider  } from '../../../../styles';

import ContactForm from './ContactForm';

import { styles, Icon, Subheading } from '@storybook/design-system';
const { breakpoint, color, spacing, typography } = styles;

const TwoColumn = styled.div`
  background: orage;
	display: flex;
  justify-content: center;
	flex-direction: row;
`;

const Column = styled.div`
  text-align: center;
  background: orage;
	width: 50%;
`;


export const Box = styled.div`
  display: block;
	background: ${props => props.themeColor};
  color: #fff;
  padding: 40px;
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  text-align: left;
	height: 25vh;
	width: 100%;

`;

export const MapWrapper = styled.div`
`;


const ContactScreen = ({mainHero, contactForm, contactInfo, contactMap,  services}) => (

				<>
				<>
					<WeHelpYouWidget class="is-height" 
				   	{...{accBtn: mainHero.action, ...mainHero}} />
        </>
				<Divider/>
				<TwoColumn>
								<Column>
												<div class="all-right">
												<h3>Direccion</h3>
												<p>{contactInfo.address[0]}</p>
												<p>{contactInfo.address[1]}</p>
												<p>{contactInfo.address[2]}</p>
												<p>{contactInfo.address[3]}</p>
												<p>{contactInfo.address[4]}</p>
												</div>
								</Column>
							 	<Column>
												<div class="all-left">
												<h3>Telefono</h3>
												<p>{contactInfo.phones[0]}</p>
												<br></br>
												<h3 style={{marginBottom: 0}}>Mail</h3>
												<p>{contactInfo.phones[1]}</p>
												</div>
								</Column>

				</TwoColumn>
				<Divider/>
				<>
								<MapWrapper>
								<iframe {...contactMap["google-map"]} frameborder="0" ></iframe>
							 </MapWrapper>
				</>

				<Divider/>
				<h1 class="hero-text">O si lo preferis, contamos en que podemos ayudarte.</h1>
				<Divider/>
				<TwoColumn>
								<Column> 
								<ContactForm {...contactForm}/>
								</Column>
				</TwoColumn>

				<Divider/>
				<> 
					<ListOfServicesWidget {...services}/>
				</>
				</>
);

const ContactScreen2 = ({mainHero, contactForm, services}) => (

				<>
				<>
			     <WeHelpYouWidget class="is-height" 
				   	{...{accBtn: mainHero.action, ...mainHero}} />
        </>
				<Divider/>
				<TwoColumn>
								<Column> <Box themeColor="indigo"/> </Column>
								<Column> <Box themeColor="indigo"/> </Column>
				</TwoColumn>

				<Divider/>
				<h1>O si lo preferis, contamos en que podemos ayudarte.</h1>
				<Divider/>

				<TwoColumn>
					<Column> <ContactScreen {...contactForm} /> </Column>
				</TwoColumn>
				<Divider/>
				<> 
					<ListOfServicesWidget {...services}/>
				</>
				</>
);



export default ContactScreen;
