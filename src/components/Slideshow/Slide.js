//===========================================
// IMPORT DEPENDENCIES
//===========================================
import React, { memo } from "react";

//===========================================
// STYLES OBJECT
//===========================================
const s = {
    container: "abs fullW fullH",
    slideImage: "fullH fullW imgCover"
};

const styles = {
	hero: {},
	title: {},
	subtitle: {}
};

const SliderCaption = (props) => (
	<div class="is-warning">
	  {props.children}
	</div>
)



//===========================================
// SLIDE FUNCTIONAL COMPONENT
//===========================================
const Slide = ({ css, position, transition, backgroundImage, children, title, subtitle  }) => {
   let backgroundStyle= {};
   if (backgroundImage) {
	   backgroundStyle["background"] = ` linear-gradient(0deg, rgba(0,184,156,0.3), rgba(0,184,156,0.3)),  url(${backgroundImage.picUri}) no-repeat center center / cover `;
   }

   let sliderCaption;
   if(children) {
     sliderCaption = (<></>);
   } else {
     sliderCaption = (
	  <SliderCaption>
		  <h1 class="title" style={{...styles.title, ...css.title}}
			  dangerouslySetInnerHTML={{__html: title}} />
		  <h2 class="subtitle" style={{...styles.subtitle, ...css.subtitle}}
			  dangerouslySetInnerHTML={{__html: subtitle}} />
	  </SliderCaption> );
   }



    return (
	<div class="hero-body" style={backgroundStyle} >
		{sliderCaption}
		{children}
        </div>
    );
};

export default memo(Slide);
/** 
<img src={backgroundImage.picUri} className={s.slideImage} alt="slide" />
 **/
