import React from 'react';

import PropTypes from 'prop-types';
import { Link } from 'gatsby';

import { JsonView } from '../../styles';


import { Noa, NoaSite } from '../../';

const BULLET = '\u2022';

export const FooterLink = (props) => (
  	<>
	  <a  style={{marginLeft: "20px", marginRight: "20px"}} href={props.link} className="has-text-primary">{props.label}</a> {BULLET}   
	</>
);


export const Footer = (props) => { 

let links;
if ( props.links ) {
	links = props.links.map((link, idx) => (
	   <FooterLink key={idx} link={link.link} label={link.label} />
	));
}



return (
<footer className="hero is-small is-light">
	<div className="hero-body">
		<div className="container has-text-centered">
			<div className="columns m-t-10">
				<div className="column">
					<nav className="has-text-grey-light">
						{ links }
					</nav>
				</div>
			</div>
			<div className="b-t m-t-30 p-t-30 has-text-grey-light is-size-7">
					{props.company.name} {props.year.year} &copy; <br/>
			</div>
		</div>
	</div>
</footer>
);
}

export default Footer;
