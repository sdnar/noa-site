import React from 'react';
import { useEffect, useState } from 'react';
import { useRef } from 'react'

import styled from 'styled-components';

import { css } from '@emotion/core';

import { Divider, Debug, JsonView, JsonPre } from '../../styles';



const styles = {
	hero: {},
	title: {},
	subtitle: {}
};


const Title = (props) => {
let klasses = ["title", "is-size-4"];
if (props.css.fontSize) {
   klasses.pop(1);
}
return  (
   <h1 className={klasses.join(" ")} style={{...props.css}}
	  dangerouslySetInnerHTML={{__html: props.children}} />
);
}

const Subtitle = (props) => {
let klasses = ["subtitle", "is-size-6"];
if (props.css.fontSize) {
   klasses.pop(1);
}

return  (
  <h2 className={klasses.join(" ")}  style={{...props.css}}
	  dangerouslySetInnerHTML={{__html: props.children}} />
);

}


const Legend = (props) => (
  <h3 className="subtitle is-size-10" style={{...props.css}}
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


export const SloganBanner = (props) => {

let owncss = {sloganStyle: {}, titleStyle: {}, subtitleStyle: {}, legendStyle: {}, ...props.css};
return (
  <>
	  <div style={owncss.sloganStyle}  >

	     <Title css={owncss.titleStyle} >{props.title}</Title>
  	     <Subtitle css={owncss.subtitleStyle} >{props.subtitle}</Subtitle>
		{props.children}
	     <Legend css={owncss.legendStyle} >{props.legend}</Legend>
  	  </div>
  </>
);
}

const SliderCaption = (props) => (
	<div style={props.css.sloganStyle} >
	  {props.children}
	</div>
)



export const NoHero = (props) => {

   let backgroundStyle= {...styles.hero};
   if (props.backgroundImage) {
     backgroundStyle["background"] = `url(${props.backgroundImage.picUri}) no-repeat center center`;
     backgroundStyle["backgroundSize"] = "cover";
   }

   let sliderCaption;
   if(props.title) {
     sliderCaption = (
	  <SliderCaption css={props.css}>
	  <h1 class="title" style={{...styles.title, ...props.css.titleStyle}} 
		dangerouslySetInnerHTML={{__html: props.title}} />
	  <h2 class="subtitle" style={{...styles.subtitle, ...props.css.subtitleStyle}}
		dangerouslySetInnerHTML={{__html: props.subtitle}} />
	  </SliderCaption> );
   } else {
     sliderCaption = (<></>);
   }

return (
   <section className="hero ">
	<div class="hero-body columns is-mobile is-centered is-vcentered" style={{...styles.hero, ...props.css.hero, ...backgroundStyle}} >
		<div class="column is-half">
		{ sliderCaption  }
		</div>
		<div class="column is-half">
		{ props.children }
		</div>
	</div>
   </section>
);
}
 


const Hero = (props) => {

   let backgroundStyle= {...styles.hero};
   if (props.backgroundImage) {
     backgroundStyle["background"] = `url(${props.backgroundImage.picUri}) no-repeat center center`;
     backgroundStyle["backgroundSize"] = "cover";
   }

   let sliderCaption;
   if(props.children) {
     sliderCaption = (<></>);
   } else {
     sliderCaption = (
	  <SliderCaption>
	  <h1 class="title" style={{...styles.title, ...props.css.title}} 
		dangerouslySetInnerHTML={{__html: props.title}} />
	  <h2 class="subtitle" style={{...styles.subtitle, ...props.css.subtitle}}
		dangerouslySetInnerHTML={{__html: props.subtitle}} />
	  </SliderCaption> );
   }

return (
   <section className="hero is-light">
	<div class="hero-body" style={{...styles.hero, ...props.css.hero, ...backgroundStyle}} >
		{ sliderCaption  }
		{ props.children }
	</div>
   </section>
);
}
 



export default Hero;
