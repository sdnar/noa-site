import React from 'react';

import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import  { JsonView } from '../../styles';

import { Iconf, Link, Subheading, styles  } from '@storybook/design-system';


const  {
				breakpoint,
				typography,
				color, 
				background, 
				pageMargins, 
				spacing, 
			  pageMargin 
} = styles;



const Title = styled.div`
  font-weight: ${typography.weight.extrabold};
  font-size: ${typography.size.s2}px;
  line-height: 1.5rem;
  
  @media (min-width: ${breakpoint * 1}px) {
    font-size: ${typography.size.m1}px;
    line-height: ${typography.size.m2}px;
    margin-bottom: 0.5rem;
  }
`;

const Desc = styled.div`
  font-size: ${typography.size.s3}px;
  line-height: 1.5;
	@media (min-width: ${breakpoint * 1}px) {
	  margin-bottom: 0.5rem;
	}
`;


const Action = styled(Link)`
	margin-right: hidden;
`; 

const Actions = styled.div``; 

const Meta = styled.div`
  overflow: hidden;
`;

const InnerWrapper = styled.div`
  display: flex;
	align-items: start;


	&:not(:last-child) {
	  margin-bottom: 2rem;
	}

	img {
	  margin-right: 20px;
		display: block;
		width: 40px;
		height: auto;
	}

	@media (min-width: ${breakpoint * 1}px) {
	  img {
			width: 48px;
		}
	}
` ;




export const Slot = ({img, heading, title, desc, actions, color, ...props}) => {
return (
<InnerWrapper>
	<img src={img} alt={title}/>
	<Meta>
		<Title>{title}</Title>
		<Desc>{desc}</Desc>
		<Actions>{ actions.map((action) => 
		    <Action withArray href={action.ref}>{action.title}</Action>
		)}
		</Actions>
	</Meta>
</InnerWrapper>
);
}

Slot.propTypes = {
	img: PropTypes.string,
	heading: PropTypes.string.isRequired,
	title: PropTypes.string,
	desc: PropTypes.string,
	children: PropTypes.oneOfType([
	  PropTypes.arrayOf(PropTypes.node),
	  PropTypes.node
	]),
	actions: PropTypes.arrayOf(PropTypes.object),
	color: PropTypes.oneOf(['green', 'seanfoam', 'gold', 'purple', 'default'])
};

Slot.defaultProps = {
  img: '',
	heading: '',
	title: '',
  desc: '', 
	actions: [],
  children: <span/>,
	color: 'default'
};

const MainWrapper = styled.div`
  background-color: ${background.app};
	border-top: 1px solid ${color.border};
	line-height: 20px;
`;

const RowWrapper = styled.div`
  display: flex;
	flex-wrap: wrap;
	flex-direction: column;
	border-bottom: 1px solid ${color.border};

	@media (min-width: ${breakpoint}px) {
    flex-direction: row;
	}
`;

const Column = styled.div`
  flex: 1;

	padding-left: ${spacing.padding.medium}px;
	padding-left: ${spacing.padding.medium}px;
	padding-top: 3rem;
	padding-bottom: 3rem;

	&:last-child {
	  border-top: 1px solid ${color.border};
	}

	@media (min-width: ${breakpoint * 1}px) {
	  &:first-child {
		  margin-left: ${pageMargin * 1}%;
			padding-right: 60px;
		}
		&:last-child {
			padding-left: 60px;
		  margin-right: ${pageMargin * 1}%;
			border-top: none;
			border-left: 1px solid ${color.border};
		}
	}

	@media (min-width: ${breakpoint * 2}px) {
    &:first-child {
		  margin-left: ${pageMargin * 2}%;
		}
		&:last-child {
		  margin-right: ${pageMargin * 2}%;
		}
	}

	@media (min-width: ${breakpoint * 3}px) {
	  &:first-child {
		  margin-left: ${pageMargins * 3}%;
		}
		&:last-child {
		  margin-right: ${pageMargin * 3}%;
		}
	}


	@media (min-width: ${breakpoint * 4}px) {
	  &:first-child {
		  margin-left: ${pageMargins * 4}%;
		}
		&:last-child {
		  margin-right: ${pageMargin * 4}%;
		}
	}

`;

const Resources = styled.div`
`;

const SlotWrapper = styled.div`
`;

export const Upper = ({slots, ...rest}) => {
return (
<MainWrapper {...rest}>
<RowWrapper>

	<Column>
			<Resources>
							<SlotWrapper>
									{slots[0]}
							</SlotWrapper>
			</Resources>
  </Column>
	<Column>
			<Resources>
							<SlotWrapper>
									{slots[1]}
							</SlotWrapper>
			</Resources>
  </Column>
</RowWrapper>
</MainWrapper>
);
}
