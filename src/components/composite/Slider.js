import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import pluralize from 'pluralize';

import { Subheading, styles  } from '@storybook/design-system';
import { Debug, JsonPre, JsonView }  from '../../styles';

import logoArsat from '../../images/logos/clients/logo-arsat.svg';

const {
	breakpoint,
	color,
	background,
	typography,
	pageMargins,
	spacing
} = styles;

const Subtext = styled(Subheading)`
  font-size: ${typography.size.s1}px;
	color: ${color.mediumdark};
	text-align: center;
`;

const Heading = styled(Subheading)`
  color: ${color.medium};
`;

const Logo = styled.img`
  display: inline-block;

	width: 100%;
	height: auto;

	max-width: 80px;
	max-height: 24px;
  
	object-fit: contain;

  @media (min-width: ${breakpoint * 1.333}px) {
	  max-width: 92px;
		max-height: 32px;
	}

	${props =>
	  props.monochrome &&
		css`
		  opacity: 0.4;
			filter: grayscale(100%);
		`};
`;

const LogoWrapper = styled.div`
  padding: 0 ${spacing.padding.medium}px;
	display: inline-block;
	text-align: center;
`;


const Logos = styled.div`
  display: flex;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;

	&:not(:only-child) {
	  padding-bottom: 0.75rem;
	}

	@media (min-width: ${breakpoint * 1.333}px) {
	  flex-wrap: wrap;
		justify-content: space-between;

		&:not(:only-child) {
		  padding-bottom: 1.5rem;
		}
	}

	&{LogoWrapper} {
	  margin-top: 0.5rem;
		margin-botttom: 0.5rem;
	}

	${props => 
	  props.grid &&
		css`
		  @media (min-width: ${breakpoint * 1.333}px) {
			  justify-content: space-evently;

				&{LogoWrapper} {
				  flex: 0 1 16.666%;
					margin-top: 1.5rem;
					margin-bottom: 1.5rem;
				}

				${Logo} {
				  opacity: 1;
				}
			}
	`};
`;

const Wrapper = styled.div`
  ${pageMargins};
	padding-top: 3rem;
	padding-bottom: 3rem;
`;

export const Slider = ({heading, path, brands, grid, monochrome, ...rest}) => {
return (

<Wrapper {...rest}>
	<Logos grid={grid}>
		{!grid && heading && <Heading>{heading}</Heading>}
		{ brands.map(brand => (
		 	<LogoWrapper key={brand} title={brand}>
		 	  <Logo src={`${path}/logo-${brand}.jpg`} alt={brand} monochrome={monochrome} /> 
		 	</LogoWrapper>
	  ))}
		</Logos>
		{grid && <Subtext>Estos son nuestros companeros de viaje.</Subtext>}
</Wrapper>

);
}


Slider.propTypes = {
  path: PropTypes.string,
  brands: PropTypes.arrayOf(PropTypes.string),
	heading: PropTypes.string,
	grid: PropTypes.bool,
	monochrome: PropTypes.bool
};

Slider.defaultProps = {
	heading: null,
	grid: false,
	monochrome: false
};

export default Slider;
