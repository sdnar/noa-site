import React from 'react';

import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import  { JsonView } from '../../styles';

import { Subheading, styles  } from '@storybook/design-system';


const  {
				breakpoint,
				typography 
				color, 
				background, 
				pageMargins, 
				spacing, 
			  pageMargin 
} = styles;

const Title = styled(Subheading)`
`;

const ResourceTitle = styled.div`
`;

