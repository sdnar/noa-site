import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import { JsonView } from '../../styles';

import { styles } from '@storybook/design-system';

const {
  breakpoint,
  typography,
  color,
  background,
  pageMargins,
  spacing,
} = styles;


const List = styled.div`
  > * {
    margin-bottom: 1.25rem;
   
		@media (min-width: ${breakpoint * 1}px) {
      margin-bottom: 1.5rem;
		}
	}
`;

const Grid = styled.div`
  display: flex;
	flex-wrap: wrap;
	flex-direction: row;
	justify-content: start;

	margin: 0 0px;

	@media (min-width: ${breakpoint * 1}px) {
	  margin: 0 -10px;
	}

	@media (min-width: ${breakpoint * 1.5}px) {
	  margin: 0 -15px;
	}

	.flex-wrapper {
	   width: 100%;

		 padding: 0 0 20px;

		 @media (min-width: ${breakpoint * 1}px) {
		   width: 50%;
			 padding: 0px 10px 20px;
		 }

		 @media (min-width: ${breakpoint * 1.5}px) {
		   width: 50%;
			 padding: 0px 15px 30px;
		 }

		 @media (min-width: ${breakpoint * 2}px) {
		   width: 33.33%;
			// width: 25%;
		 }
	}


`;

const Layout = styled.div`
  ${pageMargins};
	${props =>
	  props.appearance === 'official' &&
		css`
		  padding-bottom: calc(5rem - 40px);
		`};
	${props =>
	  props.appearance === 'oficial' &&
		css`
		  padding-bottom: 3rem;
		`};
`;

export const SlotList = ({ appearance, children, ...rest }) => {

	return(
					
	<Layout appearance={appearance} {...rest}>
			{appearance === 'official' ? <Grid>{ children }</Grid> : <List> {children} </List>}
  </Layout>

	);

}

export default SlotList;
