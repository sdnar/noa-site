import React from 'react';

import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import { JsonView } from '../../styles';

import { Iconf, Link, Subheading, styles } from '@storybook/design-system';
import { LazyLoad } from '../basics/LazyLoad';

const {
	breakpoint,
  typography,
	color,
	background,
	pageMargins,
	spacing,
	pageMargin
} = styles;

const Title = styled.div`
  font-weight: ${typography.weight.extrabold};
  font-size: ${typography.size.s2}px;
  line-height: 1.5rem;
  
  @media (min-width: ${breakpoint * 1}px) {
    font-size: ${typography.size.m1}px;
    line-height: ${typography.size.m2}px;
    margin-bottom: 0.5rem;
  }
`;

const Desc = styled.div`
  font-size: ${typography.size.s3}px;
  line-height: 1.5;
	@media (min-width: ${breakpoint * 1}px) {
	  margin-bottom: 0.5rem;
	}
`;

const Meta = styled.div`
	a {
	  display: block;
	  text-align: right;
		margin: auto;
		margin-right: 60px;
		font-weight: ${typography.weight.bold};
		color: ${color.dark};
		text-decoration: none;
	}
`;

const Image = styled.img`
  display: block;
	width: 100px;
  margin: 0 20px;

	@media (min-width: ${breakpoint * 1}px) {
	  width: 192px;
		margin-right: 30px;
  }
`;

const Inner = styled.div`
  display: flex;
  flex-direction: row;
	flex-wrap: wrap;
	align-items: center;
  text-align: left;

	padding: 3rem ${spacing.padding.medium}px;

	@media (min-width: ${breakpoint * 1}px) {
	  margin: 0 ${pageMargin * 3}%;
	}

	@media (min-width: ${breakpoint * 2}px) {
	  margin: 0 ${pageMargins * 4}%;
	}

	${Meta} {
		flex: 1;
	}
`;

export const Slot = ({img, title, desc, actionRef, actionLink}) => {
return (
   <Inner>
		{(img && <LazyLoad once height="100%"> <Image src={img} /> </LazyLoad>)}
		<Meta>
			<Title>{title}</Title>
			<Desc> {desc} </Desc>
			{(actionRef && <Link withArrow href={actionRef} > {actionLink} </Link>)}
		</Meta>
 </Inner>
);
}


const  Wrapper = styled.div`
 background: ${background.app};
 border-top: 1px solid ${color.border};
 border-bottom: 1px solid ${color.border};
 ${props =>  css`${props.appearance}`}
`;

export const OneSlot = ({children, ...rest}) => {
return (
	<Wrapper {...rest} >
			{children}
	</Wrapper>
);
}

export default OneSlot;
