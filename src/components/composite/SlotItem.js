import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Link, styles } from '@storybook/design-system';

const {
  breakpoint,
  typography,
  color,
  background,
  pageMargins,
  spacing,
} = styles;


const Image = styled.div`
  float: left;
	margin-right: 20px;

	img {
	  display: block;
		max-width: 100px;
		width: 100%;
		height: auto;

		@media (min-width: ${breakpoint * 1}px) {
		  max-width: 192px;
			margin: 0 auto;
		}
	}

	@media (min-width: ${breakpoint * 1}px) {
	  margin-bottom: 1.5rem;
		margin-right: 0;
		margin-top: 0;
		float: none;
	}
`;

const Title = styled.div`
	color: ${color.darkest};
  font-weight: ${typography.weight.extrabold};
  font-size: ${typography.size.s3}px;
  line-height: 1.5rem;

	@media (min-width: ${breakpoint * 1}px) {
	  font-size: ${typography.size.m1}px;
		line-height: ${typography.size.m2}px;
		margin-bottom: 0.5rem;
	}

	&:hover,
	&:active {
	  color: ${color.darkest};
	}
`;

const TitleLink = styled(Link)`

`;

const Desc = styled.div`
  color: ${color.dark}; 
`;

const Meta = styled.div`
  overflow: hidden;
`;


const Wrapper = styled.div``;

const WrapperLink = styled.a`
  display: block;

	height: 100%;

	font-size: ${typography.size.s3}px;
	text-decoration: none;
	overflow: hidden;

	line-height: 1.5;
	padding: 20px;

	background: ${color.lightest};
	box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 5px 5px 0 rgba(0, 0, 0, 0.05);
	border-radius: 4px;

	transition: all 150ms ease-out;
	transform: translate3d(0, 0, 0);
	cursor: pointer;

  &:hover {
	  transform: translate3d(0, -2px, 0);
		box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 5px 15px 0 rgba(0, 0, 0, 0.1);
	}

	&:active {
	  transform: translate3d(0, 0, 0);
	}

//	@media (min-width: ${breakpoint * 1}px) {
//	  padding: 2.5rem 2.5rem 3rem;
//		text-align: center;
//	}

`;

const FlexWrapper = styled.div`
// so i can get the selector in ... //
`;

export const SlotItem = ({image, title, desc, linkRef, appearance, ...rest}) => {

		if (appearance === 'official') {
			return (

				<FlexWrapper class="flex-wrapper" className="flex-wrapper">
					<WrapperLink class="wrapper-link" appearance={appearance} href={linkRef} {...rest}>
							<Image>{image}</Image>
							<Meta>
									<Title>{title}</Title>
									<Desc>{desc}</Desc>
							</Meta>
					</WrapperLink>
				</FlexWrapper>

		  );
		}

		return (
						
			<Wrapper {...rest}>
          <Meta>
							<TitleLink hreef={linkRef} rel="nofollow">
									{title}
							</TitleLink>
							<Desc>{desc}</Desc>
					</Meta>
			 </Wrapper>

		);
}

export default SlotItem;
