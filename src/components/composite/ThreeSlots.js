import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import { JsonView  } from '../../styles';

import { Iconf, Link, Subheading, styles } from '@storybook/design-system';

const {
  breakpoint,
  typography,
  color,
  background,
  pageMargins,
  spacing
} = styles;


export const Slot = () => <></>;

export const ThreeSlots = () => <></>;

export default ThreeSlots; 


