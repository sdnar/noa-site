import React from 'react';

import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import { JsonView } from '../../styles';

import  { Subheading, styles } from '@storybook/design-system';

const { color, typography, spacing, pageMargins, breakpoint } = styles;

export const Heading = styled(Subheading)`
  display: block;
	margin-bottom: 1rem;
	color: ${color.mediumdark};
	opacity: 0.8;

  ${props => (props.color === 'seafoam') && css` color: ${color.seafoam}; `};
  ${props => (props.color === 'green'   ) && css` color: ${color.green}; `};
  ${props => (props.color === 'gold'    ) && css` color: ${color.gold}; `};
  ${props => (props.color === 'purple'  ) && css` color: ${color.purple}; `};
`;

export const Title = styled.h1`
  font-size: ${typography.size.m3}px;
	font-weight: ${typography.weight.black};
	line-height: 24px;
	maring-bottom: 0.5rem;
	text-align: center;

	@media (min-width: ${breakpoint}px) {
	  font-size: ${typography.size.l2}px;
		line-height: 1;
		maring-bottom: 1rem;
	}
`;

export const Desc = styled.div`
  font-size: ${typography.size.s3}px;
	line-height: 1.5;
	color: ${color.dark};
	margin-top: 15px !important;

	@media (min-width: ${breakpoint}px}) {
	  font-size: ${typography.size.m1}px;
		line-height: 32px;
	}
`;

export const Meta = styled.div`
  margin-left: auto;
	margin-right: auto;
	text-align: center;

	@media (min-width: ${breakpoint}px) {
	  max-width: 600px;

		${Desc} {
		  margin: 0 auto;
		}
	}

`;

export const Wrapper = styled.div`
  ${pageMargins};
	padding-top: 3rem !important;
	padding-bottom: 3rem !important;

	text-align: center;

	@media (min-width: ${breakpoint}px) {
	  padding-top: 7rem !important;
		padding-bottom: 5rem !important;
	}

`;

export const PageTitle = ({heading, title, desc, color: headingColor, ...props }) =>  {
	return (
		<Wrapper {...props}>
				<Meta>
					<Heading color={headingColor}>{heading}</Heading>
					<Title>{title}</Title>
					<Desc>{desc}</Desc>
				</Meta>
    </Wrapper>
	);
};

PageTitle.propTypes = {
  heading: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	desc: PropTypes.string.isRequired,
	color: PropTypes.oneOf(['green', 'seanfoam', 'gold', 'purple', 'default']),
};

PageTitle.defaultProps = {
  color: 'default',
};

export default PageTitle;
