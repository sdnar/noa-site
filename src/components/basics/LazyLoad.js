import React from 'react';
import PropTypes from 'prop-types';
import ReactLazyLoad from 'react-lazyload';


export const LazyLoad = ({ children, ...props }) => {
	if (LazyLoad.disabled) {
		return children;
	}

	return <ReactLazyLoad {...props}>{children}</ReactLazyLoad>
}

LazyLoad.propTypes = {
	children: PropTypes.node.isRequired
};

export default LazyLoad;
