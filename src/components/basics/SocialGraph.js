import React from 'react';

import PropTypes from 'prop-types';

import Helmet from 'react-helmet';

import styled from 'styled-components';

import { styles } from '@storybook/design-system';



export const Graph = ({title, desc, url, image}) => (
<Helmet title={title}>
  {desc && <meta name="description" content={desc}/>}
  {url && <meta name="org:url" content={url}/>}
  {image && <meta property="og:image" content={image}/>}

   <meta property="og:title" content={title} />
   {desc  && <meta property="og:description" content={desc} />}
   <meta name="twitter:title" content={title}/>
   {image && <meta name="twitter:image" content={image}/>}
   
</Helmet>
);

Graph.propTypes = {
 title: PropTypes.string,
 desc: PropTypes.string,
 url: PropTypes.string,
 image: PropTypes.string
};

Graph.defaultProps = {
  title: "ALGUN TITULO",
  desc: null,
  url: null,
  image: null
};

export default Graph;
