import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'gatsby';

import Noa from '../../Noa';
import NoaSite from '../../NoaSite';

const Header = (props) => { 

let links = props.links.map((link, idx) => {
	if(link.featured == true) {
	   return ( <div key={idx} className="navbar-item">
		<a href={link.link} className="button is-primary has-shadow">{link.label}</a>
		</div>);
	} else {
	   return ( <a key={idx} href={link.link} className="navbar-item has-text-weight-semibold">{link.label}</a>);
	}
});

return (
	<div className="hero-head">
		<nav className="navbar is-transparent is-spaced" role="navigation" aria-label="main navigation">
			<div className="container">
				<div className="navbar-brand">
				  <a className="navbar-item has-text-weight-bold" href="/">
						<img src={NoaSite._metadata.logo.picUri} alt={NoaSite._metadata.title} width="" height="20"/>
							{NoaSite._metadata.name}
					</a>

					<a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarTopMain">
						<span aria-hidden="true"></span>
						<span aria-hidden="true"></span>
						<span aria-hidden="true"></span>
					</a>
				</div>
				<div className="navbar-menu" id="navbarTopMain">
					<div className="navbar-end">
							{links}
					</div>
				</div>
			</div>
		</nav>
	</div>
);

}

export default Header;

