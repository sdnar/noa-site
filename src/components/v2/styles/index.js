import generalStyles from './general.module.css';
import contentStyles from './content.module.css';
import blockStyles from './content.module.css';
import blockImageStyles from './block.image.module.css';

export const styles = {
			...generalStyles,
			...contentStyles,
			...blockStyles, ...blockImageStyles
};


export default styles;
