import React from 'react';
import styled from 'styled-components';

import { css } from '@emotion/core';

import { Debug, JsonView, JsonPre } from '../../../styles';


const Title = (props) => (
   <h1 className="title"
	  dangerouslySetInnerHTML={{__html: props.children}} />
);

const Subtitle = (props) => (
  <h2 className="subtitle default" 
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


const Legend = (props) => (
  <h3 className="subtitle default"
	  dangerouslySetInnerHTML={{__html: props.children}} />
);


export const SloganBanner = ({title, subtitle, legend, children}) => (
  <section class="sloganhero">
	  <div className="contentRow row is-bottom-left" >
						<div className="column small-12 medium-6">
						<div className="content">
	     <Title>{title}</Title>
  	   <Subtitle>{subtitle}</Subtitle>
						{children}
	     <Legend>{legend}</Legend>
  	  </div>
  	  </div>
  	  </div>
  </section>
);

export default SloganBanner;
