import React from 'react';
import styled from 'styled-components';


import { Debug, JsonView, JsonPre } from '../../../styles';
import { Noa, NoaSite } from '../../../';

import Pitch from '../../../v3/components/screens/IndexScreen/Pitch';
import Guides from '../../../v3/components/screens/IndexScreen/Guides';
import Guide from '../../../v3/components/screens/IndexScreen/Guide';

const styles = {
   ListContainer: { maxWidth: "85rem", minHeight: "120px", alignItems: "center", flexWrap: "wrap", justifyContent: "space-around" }
};


/***  LIST BANNER ****/

export const ItemSubtitle = (props) => (
		<h2 className="subtitle" style={{ fontSize: "1rem",    fontWeight: "400"}}>{props.children} </h2>
)

export const ItemTitle = (props) => (
		<h2 className="title" style={{ fontSize: "1rem",   fontWeight: "600", color: "#3273dc" }}> {props.children} </h2>
);

/* <ItemHeader> </ItemHeader> */

export const Item  = (props) => {


return (
   <div className="" >
  	   <figure  > <img src={props.image} alt={props.name} style={props.css}/> </figure>
	    <ItemTitle>{props.title}</ItemTitle>
	    <ItemSubtitle>{props.subtitle}</ItemSubtitle>
   </div>

);
}

export const ItemBox  = (props) => {
let ownclasses = ["column is-one-third vcentered "];
if(props.css && props.css.itemBoxClasses)  {
   ownclasses = [...ownclasses, ...props.css.itemBoxClasses];
}
return (
   	<div className={ownclasses.join(" ")}>
	  {props.children}
	</div>
);
}

export const ListOfItems = (props) => {
  let owncss = {listContainerStyle: {}, ...props.css};

  let items;
  if( props.items ) {
        items =  props.items.map((item, idx) => (
     	<ItemBox key={idx} css={{...props.css}} >
		<Item 
		      title={item.title}
		      subtitle={item.overview} 
		      image={item.image}
		      css={{...item.css}} />
     	</ItemBox>));
  }


  return (
   	<div  className="is-full column columns is-centered " >
        <div className="is-full column columns is-multiline is-centered is-vcentered" style={{...styles.ListContainer, ...owncss.listContainerStyle}} >
        { items }
        { props.children }
        </div>
        </div>
  );
}


export const FeatureList = {};


export default FeatureList;

/*
	<ListWidget 
		title={props.title} subtitle={props.subtitle} legend={props.legend} css={props.css}
		items={ props.services.map(service => {
			return { title: service.name, subtitle: service.overview, image: service.media.logos[0] };
		})} >

	</ListWidget>
*/

/* <ListOfItems items={props.items} css={props.css} /> */

