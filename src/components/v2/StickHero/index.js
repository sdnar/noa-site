import React from 'react';


export const StickHero = ({title, subtitle, legend, background}) => {

return (
				<section class="stickhero">
								<div  className="contentRow row is-bottom-left">
												<div className="column small-12 medium-6">
																<div className="content">
																				<h4 className="subtitle default"></h4>
																				<h1 className="title default"
																								dangerouslySetInnerHTML={{__html: title}}/>
																				<h3  className="legend"
																								dangerouslySetInnerHTML={{__html: legend}}/>
																</div>

												</div>
								</div>
				</section>	
)
};

export default StickHero;
