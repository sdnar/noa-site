import React from 'react';

// import styles from './styles.css';
// import './styles.css';
// import styles from '../styles';

// import { JsonView, JsonPre, Line, Divider } from '../../../styles';

import '../styles/general.module.css';
import '../styles/content.module.css';
import '../styles/block.module.css';
import '../styles/block.image.module.css';

function doBackground({image}) {
	 return {
					 background: ` linear-gradient(0deg, rgba(0,184,156,0.3), rgba(0,184,156,0.3)), url(${image.picUri}) no-repeat center center / cover`,
					 backgroundSize: "cover"
	 }
}


export const Hero = ({title, subtitle, legend, background }) => {

return (
<section className="hero has-background-image block showAlways image bgTop bgCover dark width50 default bgWhite video" style={doBackground(background)} >
  <div  className="contentRow row is-bottom-left">
    <div className="column small-12 medium-6">
      <div className="content">
        <h4 className="subtitle default"></h4>
	 			<h1 id="xxl" className="xxl title default"
						dangerouslySetInnerHTML={{__html: title}}/>
				<h3  className="legend"
					  dangerouslySetInnerHTML={{__html: legend}}/>
	    </div>
	  </div>
  </div>
</section>
);
};

export default Hero;
