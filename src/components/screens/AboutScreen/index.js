import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';

import { JsonView } from '../../../styles';

import  { Subheading, styles } from '@storybook/design-system';

import PageTitle from '../../composite/PageTitle';
import { Upper as  TwoSlot, Slot } from '../../composite/Upper';
import { OneSlot, Slot as FullSlot  } from '../../composite/OneSlot';

import MisionSVG from '../../../images/colored-icons/pixel.svg';
import VisionSVG from '../../../images/colored-icons/projects.svg';
import CustomSVG from '../../../images/community/bug.svg';

const { color, typography, spacing, pageMargins, breakpoint } = styles;

const Heading = styled.h2`
  font-weight: ${typography.weight.extrabold};
`;

const Subheader = styled(Subheading)`
  
`;

const OwnObjetive = styled(OneSlot)`
  margin-bottom: 5rem;
`;

const [ empresaSlot, misionSlot, visionSlot, objetivoSlot ] = [
  { heading:"LA EMPRESA", title:"Creando Oportunidades de Negocio", desc:"SOFT del NOA nace en 2001 de un grupo de profesionales con una gran experiencia en Desarrollo e Implementacion de Software y Consultoria IT.", color:"default"},
  { img: MisionSVG , heading: "", title: "Nuestra Mision", desc: "Crear oportunidades de negocio para nuestros clietnes a traves de la utilizacion de tecnologias y metodologias de ultima generacion.", children: "", color: "" },
	{ img: VisionSVG , heading: "", title: "Nuestra Vision", desc: "Ser reconocidos como una empresa orientada a la calidad e innovacion continua que satisface las necesidades particulares de nuestros clientes.", children: "", color: "" },
	{ img: CustomSVG , heading: "", title: "Nuestro Objetivo", desc: "Los perfiles profesionales con los que contamos en Soft del Noa nos permiten administrar de forma calificada y competitiva diversas tecnicas de analisis, metodologias de desarrollo, sistemas operativos, bases de datos y lenguajes de programacion.", children: "", color: "", actionRef: "#", actionLink:  "Ing. Sergio Paz" },
];

export const PureAboutScreen = () => {

return (
<>
  <PageTitle {...empresaSlot} />

	<TwoSlot  slots={[<Slot {...misionSlot} />, <Slot {...visionSlot}/>]}/>
	<OneSlot ><FullSlot {...objetivoSlot}/></OneSlot>


  <PageTitle heading="" title="" desc="" color="default" />
</>
);
};

PureAboutScreen.propTypes = {};

export const AboutScreen = () => {

 return (<PureAboutScreen>ABOUT</PureAboutScreen>);
}

export default AboutScreen;
