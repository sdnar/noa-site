import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';

import { JsonView } from '../../../styles';

import  { Subheading, styles } from '@storybook/design-system';

import PageTitle from '../../composite/PageTitle';
import { Upper as  TwoSlot, Slot } from '../../composite/Upper';
import { OneSlot, Slot as FullSlot  } from '../../composite/OneSlot';
import { SlotList } from '../../composite/SlotList';
import { SlotItem } from '../../composite/SlotItem';

import MisionSVG from '../../../images/colored-icons/pixel.svg';
import VisionSVG from '../../../images/colored-icons/projects.svg';
import CustomSVG from '../../../images/community/bug.svg';

import ListOfServicesWidget from '../../../fragments/ListOfServicesWidget';
import { servicesSection  } from '../../../data/commons.db';

import Stats  from '../ServiceScreen/Stats';
import { db as servicesDB } from '../../../pages/services';

import SliderOfClients from '../../composite/Slider';


const { color, typography, spacing, pageMargins, breakpoint } = styles;

const { stats: businessStats } = servicesDB;

const OwnObjetive = styled(OneSlot)`
  margin-bottom: 5rem;
`;

const [ empresaSlot, misionSlot, visionSlot, objetivoSlot, pharseSlot ] = [
  { heading:"", title:"QUIENES SOMOS", desc:"SOFT del NOA nace en 2001 de un grupo de profesionales con una gran experiencia en Desarrollo e Implementacion de Software y Consultoria IT.", color:"default"},
  { img: MisionSVG , heading: "", title: "Nuestra Mision", desc: "Crear oportunidades de negocio para nuestros clietnes a traves de la utilizacion de tecnologias y metodologias de ultima generacion.", children: "", color: "" },
  { img: VisionSVG , heading: "", title: "Nuestra Vision", desc: "Ser reconocidos como una empresa orientada a la calidad e innovacion continua que satisface las necesidades particulares de nuestros clientes.", children: "", color: "" },
  { img: CustomSVG , heading: "", title: "Nuestro Objetivo", desc: "Formar un equipo de profesionales con que administren de forma calificada y competitiva diversas tecnicas de analisis, metodologias de desarrollo.", children: "", color: "", actionRef: "#", actionLink:  "Ing. Sergio Paz" },
{ img: "" , heading: "", title: "Creando Oportunidades de Negocio", desc: "Formar un equipo de profesionales con que administren de forma calificada y competitiva diversas tecnicas de analisis, metodologias de desarrollo.", children: "", color: "", actionRef: "#", actionLink:  "Ing. Sergio Paz" },

];


const Heading = styled.h2`
  color: ${color.darkmedium};
  font-weight: ${typography.weight.extrabold};
  font-size: ${typography.size.m1}px;
  line-height: 1;

  padding-bottom: 1rem;

  @media (min-width: ${breakpoint * 1}px) {
    font-size: ${typography.size.m2}px;
    padding-bottom: 2rem;
  }

`;

const Subheader = styled(Subheading)`
  
`;


const PageMargins = styled.div`
  ${pageMargins};
`;

const StatsWrapper = styled.div`
  margin-bottom: 70px;   
`;

const OneWrapper = styled.div`
  width: 50% ;
  margin: auto;
`;

const ClientsWrapper = styled.div``;

export const PureAboutScreen = () => {

return (
<>
  <PageTitle {...empresaSlot} />
  <PageMargins>
	<Heading>Nuestra Identidad Corporativa</Heading>
  </PageMargins>
  <SlotList appearance="official">
	  <SlotItem appearance="official" image={<img src={misionSlot.img} alt="mision"/>} {...misionSlot} />
	  <SlotItem appearance="official" image={<img src={visionSlot.img} alt="mision"/>} {...visionSlot} />
	  <SlotItem appearance="official" image={<img src={objetivoSlot.img} alt="mision"/>} {...objetivoSlot} />
  </SlotList>
</>
);
};

/* <ListOfServicesWidget {...servicesSection}/> */
PureAboutScreen.propTypes = {};

export const AboutScreen = () => {

 return (<PureAboutScreen>ABOUT</PureAboutScreen>);
}

export default AboutScreen;
