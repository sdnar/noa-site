import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Guides from '../../../v3/components/screens/IndexScreen/Guides';

export const Services   = ({services}) => {

const nodes = services.map(({title, overview, client, themeColor, thumbImagePath, image, slug}) => {
	return {
		node: {
			fields: {
				guide: '#',
				slug: slug
			},
			frontmatter: {
				description: overview,
				title: title,
				chapter: client,
				themeColor: themeColor,
				thumbImagePath: image
			}
		}
	};
});

const guidesProps = {
  chaptersEdges: [
    {
      node: {
        fields: {
          guide: 'guide',
        },
      },
    },
  ],
  guidesEdges: nodes,
};

return (
  <Guides {...guidesProps}/>
);
}



export default Services;
