import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import pluralize from 'pluralize';

import { styles } from '@storybook/design-system';

import { Debug, JsonView, JsonPre } from '../../../styles';

import Stat from './Stat';


import andcountingImage from '../../../assets/images/lsb-andcounting.svg';

const { breakpoint, pageMargins, color, typography } = styles;

const StatsWrapper = styled.div`
 ${pageMargins}

 && {
   margin-top: 66px;
 }

 @media (min-width: ${breakpoint * 1.25}px) {
   display: flex;
 }
`;

const StatWrapper = styled.div`
  position: relative;
  margin-top: 30px;
  
  @media (min-width: ${breakpoint * 1.25}px) {
    margin-right: 66px;
    margin-top: 0;
  }

  @media (min-width: ${breakpoint * 1.75}px) {
    margin-right: 97px;
  }

  &:last-of-type {
    maring-right: 0;
  }
`;

const AndCountingImage = styled.img.attrs({ src : andcountingImage  })`
  position: absolute;
  top: 0;
  left: ${props => (props.withMultiple ? 160 : 108)}px; 

  @media (min-width: ${breakpoint * 1.25}px ) {
    width: 90px;
    top: 5px;
  }

  @media (min-width: ${breakpoint * 1.75}px ) {
    width: auto;
    top: 0; 
  }
`;

export const Stats = ({ stats }) => {


return (
	<StatsWrapper>
	{ stats.map(({name, count, message, more}) => (
		<StatWrapper>
			<Stat heading={pluralize(name, count, true)} message={message}/> 
			{ more==1 && <AndCountingImage withMultiple={true} /> }
		</StatWrapper>
	)) }
	</StatsWrapper>
);
}

export default Stats;
