import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Debug, JsonView, JsonPre } from '../../../styles';
import { Line, Divider } from '../../../styles';

import dots from '../../../assets/images/bg-dots.svg';

// import { StaticQuery, graphql } from 'gatsby';
import { Button, styles } from '@storybook/design-system';

// import { GatsbyLink } from '../../basics/GatsbyLink';

// import CTA  from '../../composite/CTA'; 

import Pitch from './Pitch';
import Services from './Services';
import ServicesStats from './Stats';
// import ClientValidation from './ClientValidation';
// import  WhatIsNoa from './WhatIsNoa';

const { breakpoint, color, pageMargin } = styles;

/* background: linear-gradient(to top, rgba(245, 249, 252, 1) 25%, transparent),  url('${dots}?t=${props => props.time}') no-repeat center center; */
const DotBackground = styled.div`
   background: url('${dots}?t=${props => props.time}') no-repeat center center;
   background-repeat: repeat-x;
   background-position-y: 20vh;
`;


const BottomSection = styled.div`
  padding: 84px 20px 1rem;

  @media: (min-width: ${breakpoint * 1}px) {
    margin: 0 ${pageMargin * 3}%;
  }

  @media (min-width: ${breakpoint * 2}px) {
    margin: 0 {pageMargin * 4}%;
  }

`;

const ClientValidationLineBreak = styled.div`
  height: 1px;
  background: ${color.mediumlight};

  @media (min-width: ${breakpoint}px) {
    margin-left: -56px;
    margin-right: -56px;
  }
`;

/* <JsonView src={data} name="ServiceScreen"/> */

const IndexScreen = ({data}) => {

const {pick, services, stats } = data;

return (
<> 
<Line/>
<DotBackground time={Date.now()} id="dot-background">
  <Pitch {...pick} />
  <Services services={services} />
  <ServicesStats stats={stats} />
</DotBackground>
<Divider/>
</>
);
}

export default IndexScreen;


/*
const IndexScreen = ({ data }) => (
<>
  <DotBackground time={Date.now()}>
    <Pitch />
    <Services services={data.services} />
    <ServicesStats stats={data.servicesStats} />
  </DotBackground>

  <WhatIsNoa {data.about} />
   
  <ClientValidation clients={data.clients} testimonials={data.testimonials} />
  <ClientValidationLineBreak />
  
  <BottomSection>
    <CTALineBreak/>
    <CTA
      text={data.cta.text}
      action={
      <GatsbyLink to={data.cta.actionTo}>
    	  <Button apperance="secondary">{data.cta.actionText}<button>
     </GatsbyLink>
     }
     />
  </BottomSection>
</>
);
*/
