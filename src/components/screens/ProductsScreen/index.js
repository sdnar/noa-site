import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Debug, JsonView, JsonPre } from '../../../styles';
import { Line, Divider } from '../../../styles';

import dots from '../../../assets/images/bg-dots.svg';

import { Button, styles } from '@storybook/design-system';

const { breakpoint, color, pageMargin } = styles;

const DotBackground = styled.div`
   background: url('${dots}?t=${props => props.time}') no-repeat center center;
   background-repeat: repeat-x;
   background-position-y: 20vh;
`;


const BottomSection = styled.div`
  padding: 84px 20px 1rem;

  @media: (min-width: ${breakpoint * 1}px) {
    margin: 0 ${pageMargin * 3}%;
  }

  @media (min-width: ${breakpoint * 2}px) {
    margin: 0 {pageMargin * 4}%;
  }

`;

const ClientValidationLineBreak = styled.div`
  height: 1px;
  background: ${color.mediumlight};

  @media (min-width: ${breakpoint}px) {
    margin-left: -56px;
    margin-right: -56px;
  }
`;

const IndexScreen = ({data}) => {

const {pick, services, stats } = data;

return (
<> 
<Line/>
<DotBackground time={Date.now()} id="dot-background">
</DotBackground>
<Divider/>
</>
);
}

export default IndexScreen;
