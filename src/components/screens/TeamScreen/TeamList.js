import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';

import { styles } from '@storybook/design-system';

const {
  breakpoint,
  color, 
  background,
  typography,
  pageMargins,
} = styles;

const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  justify-content: start;

  margin: 0 0px;

  @media (min-width: ${breakpoint * 1}px) {
    flex-direction: row;
  }

  > * {
    padding-bottom: 2rem;

    @media (min-width: ${breakpoint * 1}px) {
      width: 33.333%;
      padding: 3rem 20px;

      &:not(:nth-child(3n)) {
	border-right: 1px solid ${color.border};
      }

      &:nth-child(-n + 3) {
        border-bottom: 1px solid ${color.border};
	padding-top: 1.5rem;
      }

      &:nth-child(n + 3) {
        padding-bottom: 1.5rem;
      }
    }
 }
`;

const Layout = styled.div`
  ${pageMargins};
  padding-bottom: calc(5rem - 40px);

  @media (min-width: ${breakpoint * 1}px) {
    margin: 0 ${pageMargins * 2}%;
  }

  @media (ming-width: ${breakpoint * 2}px) {
    margin: 0 ${pageMargins * 3}%;
  }
`;

export const TeamList = ({ children, ...rest }) => {
return (
  <Layout {...rest}>
     <Grid >{children}</Grid>
  </Layout>
);
}



export default TeamList;


