import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';

import { graphql, StaticQuery } from 'gatsby';

import { JsonView } from '../../../styles';

import { styles } from '@storybook/design-system';

import SocialGraph from '../../basics/SocialGraph';
import PageTitle from '../../composite/PageTitle';

import TeamItem from './TeamItem';
import TeamList from './TeamList';
import ContributorItem from './ContributorItem';

const { breakpoint, color, background, typography, pageMargins  } = styles;

const path = "static/images/logos/team";

const query = graphql`
query Team {
  site { 
    siteMetadata {
      team {
        ...teamData
      }
    }
  }
}

fragment teamData on SiteSiteMetadataTeam {
  sergio { name, title, bio  },
  mario { name, title, bio  },
  sandra { name, title, bio  },
  vero { name, title, bio  },
  jose { name, title, bio  }
  tribu, count
}
`;

export const Team = styled(TeamList)`
width: 80%; 
max-width: 80%;
@media (min-width: ${breakpoint * 1}px) {
  margin: auto;
  margin-bottom: 5rem;
  width: 80%; 
  max-width: 80%;

}
`;

export const PureTeamScreen = ({data, ...rest}) => {

const { sergio, mario, sandra, vero, jose, count, tribu } = data.team;

return (
<>
  <SocialGraph 
	title={`Quienes Somos ?`}
	desc=""
	url=""
        image=""
  />
  <PageTitle heading="Nuestro Equipo"  />
  <Team>
	  <TeamItem {...{avatarUrl: `${path}/sergio.jpg`, phone: "42283211", email: "@mail", ...sergio }}/>
	  <TeamItem {...{avatarUrl: `${path}/mario.jpg`,  phone: "42283211", email: "@mail", ...mario}}/>
	  <TeamItem {...{avatarUrl: `${path}/sandra.jpg`, phone: "42283211", email: "@mail", ...sandra}}/>
	  <TeamItem {...{avatarUrl: `${path}/vero.jpg`,   phone: "42283211", email: "@mail", ...vero}}/>
	  <TeamItem {...{avatarUrl: `${path}/jose.jpg`,   phone: "42283211", email: "@mail", ...jose}}/>
	  <ContributorItem 
		  contributors={tribu.map(name => {return {name: name, avatarUrl: `${path}/${name}.jpg`}})} 
		  contributorCount={`+${count}`}
	  />
  </Team>
</>
);
}

PureTeamScreen.propTypes = {
  data: PropTypes.any
};

//  data: { team: defaultTeam } //

PureTeamScreen.defaultProps = {
  data: null
}

export const TeamScreen = ({...props} ) => {
return (
  <StaticQuery 
	query={query}
	  render={(data) =>  ( <PureTeamScreen data={{team: data.site.siteMetadata.team }} /> )}
  />	
);
}

export default TeamScreen;
