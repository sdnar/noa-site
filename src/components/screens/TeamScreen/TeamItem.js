import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Avatar, Icon, Link, styles } from '@storybook/design-system';
import { LazyLoad } from '../../basics/LazyLoad';

const {
	breakpoint,
	color,
	background,
	typography,
	pageMargins
} = styles;


const Profile = styled(Avatar)`
  height: 80px;
  width: 80px;
  
  @media (min-width: ${breakpoint * 1}px) {
    height: 120px;
    width: 120px;
  }
`;

const ImageOuter = styled.div`
  margin-right: 20px;

  @media (min-width: ${breakpoint * 1}px) {
    margin-right: 0;
    margin-bottom: 1.25rem;
  }
`;

const Name = styled.div`
  font-size: ${typography.size.s3}px;
  font-weight: ${typography.weight.extrabold};
  color: ${color.darkest};
  margin-bottom: 0.25rem;
`;
const Title = styled.span``;
const Company = styled.span``;
const Location = styled.div``;
const Overview = styled.div`
 margin-top: 0.25px;

 @media (min-width: ${breakpoint * 1}px) {
   margin-top: 0.5rem;
 }

 > a {
   margin-right: 15px;}

   @media (min-width: ${breakpoint * 1}px) {
     margin: 0 7.5px;
   }

   svg {
     height: 1rem;
     width: 1rem;
   }
 }
`;
const Social = styled.div`
 margin-top: 0.25px;

 @media (min-width: ${breakpoint * 1}px) {
   margin-top: 0.5rem;
 }

 > a {
   margin-right: 15px;}

   @media (min-width: ${breakpoint * 1}px) {
     margin: 0 7.5px;
   }

   svg {
     height: 1rem;
     width: 1rem;
   }
 }
`;

const Meta = styled.div`
  color: ${color.dark}
`;

const Item = styled.div`
  font-size: ${typography.size.s2}px;
  line-height: 20px;
  position: relative;
  display: block;
  text-align: left;

  display: flex;
  flex-direction: row;

  @media (min-width: ${breakpoint * 1}px) {
    flex-direction: column;
    text-align: center;
  }
`;



export const TeamItem = ({
  name, title, bio, overview, company, companyUrl, location, avatarUrl, hubUrl, email, phone,
  ...rest }) =>  {

	

return(
  <Item {...rest}>

   <ImageOuter>
     <LazyLoad once placeholder={<Profile loading />} height="100%">
       <Profile size="large" username={name} src={avatarUrl} />
    </LazyLoad>
   </ImageOuter>

   <Meta>
     <Name>{name}</Name>
     <Title>
       {title}
       {company && companyUrl && (
         <Company>
             {' at ' }
             <Link secondary href={companyUrl} target="_blank">
               <b>{company}</b>
             </Link>
         </Company>
      )}
     </Title>
	     {bio && <Overview>{bio}
	     </Overview>}
      {location && (<Location>{location}</Location>)}
      {(hubUrl || phone || email) && 
      <Social>
      { hubUrl && ( <Link tertiary href={hubUrl} target="_blank" containsIcon> <Icon icon="web"/> </Link> ) }
      { phone  && ( <Link tertiary href={phone} target="_blank" containsIcon> <Icon icon="phone"/> </Link> ) }
      { email  && ( <Link tertiary href={email} target="_blank" containsIcon> <Icon icon="email"/> </Link> ) }
      </Social>}
   </Meta>

   

  </Item>
);

};


TeamItem.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  overview: PropTypes.string,
  bio: PropTypes.string,
  company: PropTypes.string,
  companyUrl: PropTypes.string,
  location: PropTypes.string,
  avatarUrl: PropTypes.string,
  hubUrl: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string
};

TeamItem.defaultProps = {
  name: "#NAME",
  title: null,
  bio: null,
  overview: null,
  company: null,
  companyUrl: null,
  location: null,
  avatarUrl: null,
  hubUrl: null,
  email: null,
  phone: null
};

export default TeamItem;

