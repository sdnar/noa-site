import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Avatar, Link, styles } from '@storybook/design-system';

const {
 breakpoint,
 color,
 background,
 typography,
 pageMargins
} = styles;


const Heading = styled.div`
  font-size: ${typography.size.s3}px;
	font-weight: ${typography.weight.extrabold};
	color: ${color.darkest};
	margin-bottom: 2rem;
`;

const Contributor = styled(Avatar)`
`;

const Contributors = styled.div`
  margin: -5px;

	&{Contributor} {
	  margin: 5px;
	}
`;
const ContributorsWrapper = styled.div`
	margin-bottom: 1rem;

	@media (min-width: ${breakpoint * 1}px) {
	  width: 140px;
		margin: 0 auto 1rem;
  }
`;

const Wrapper = styled.div`
  font-size: ${typography.size.s2}px;
	line-height: 20px;
	position: relative;
	display: block;

	@media (min-width: ${breakpoint * 1}px) {
	  text-align: center;
	}
`;

export const ContributorItem = ({contributors, contributorCount, hubUrl, ...rest}) => {
return (
<Wrapper {...rest}>
<Heading>{contributorCount} tiburones</Heading>
<ContributorsWrapper>
  <Contributors>
	{ contributors.map(({name, avatarUrl}) => 
			<Contributor key={name} size="large" username={name} src={avatarUrl} title={name}/>
	)}
	</Contributors>
</ContributorsWrapper>
</Wrapper>
);
}
				/*
{ contributors && (
<Link withArray href={hubUrl}>
	Unite al Equipo
</Link>
)}
*/


ContributorItem.propTypes = {
 contributors: PropTypes.arrayOf(
   PropTypes.shape({
     name: PropTypes.string,
     avatarUrl: PropTypes.string
   })
 ),
 contributorCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 hubUrl: PropTypes.string
};

ContributorItem.defaultProps = {
	contributors: [],
	contributorsCount: 0,
	hubUrl: null,
};

export default ContributorItem;
