import React from 'react';
import { useEffect, useState } from 'react';
import { useRef } from 'react'

import styled from 'styled-components';

import { css } from '@emotion/core';

import { Divider, Debug, JsonView, JsonPre } from '../../styles';

import Slideshow from '../Slideshow';

const styles = {
	hero: {},
	title: {},
	subtitle: {}
};


const SliderCaption = (props) => (
	<div class="is-warning">
	  {props.children}
	</div>
)

const HeroCarousel = (props) => {


return (
   <section className="hero is-height is-light">
	<Slideshow slides={props.slides} />
   </section>
);
}
 
export default HeroCarousel;
