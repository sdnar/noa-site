import React from 'react';
import styled from 'styled-components';

import { css } from '@emotion/core';

import { Debug, JsonView, JsonPre  } from '../../styles';

export const Avatar = (props) => (
   <div>
	<figure className="is-16by9 is-inline-block" >
	<img src={props.image} alt={props.name} style={props.css}/>
	</figure>
   </div>
);

export default Avatar;
