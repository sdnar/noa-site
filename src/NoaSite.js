import Noa from  './Noa';

import NoaMedia from './NoaMedia';

import {
				homePage,
				consultoriaPage,
				digitalbankPage,
				contactPage,
				servicesPage
} from './data';

export const headerLinks = [
					{ label: "Nosotros", link: "/about", alt: "" },
					{ label: "Servicios", link: "/services", alt: "" },
					{ label: "Productos", link: "http://192.168.1.65:8001", alt: "" },
			    { label: "Contacto", link: "/contact", alt: "", featured: true }
];

export const footerLinks = [

			{ label: "Nosotros", link: "/about", alt: "" },
			{ label: "Servicios", link: "/services", alt: "" },
			{ label: "Productos", link: "http://192.168.1.65:8001", alt: "" },
			{ label: "Contacto", link: "/contact", alt: "", featured: true }
];

const NoaSite = {
	_metadata: {

	  noa: Noa,
	  author: Noa.ouwner,
	  logo: Noa.logo,
	  name: Noa.name,
	  title: Noa.overview,
	  description: Noa.summary

	},
	global: {
	   headerLinks: headerLinks,
	   footerLinks: footerLinks,
	   footerMark: { name: Noa.name },
	   footerYear: { year: "2019" }
	},
	pages: {
		 home: homePage,
		 consultoria: consultoriaPage,
		 digitalbank: digitalbankPage ,
		 contact: contactPage,
		 services: servicesPage
	}
}

export default NoaSite;
