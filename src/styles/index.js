import React from 'react';
import styled from "styled-components";

import ReactJson from 'react-json-view';


export const Debug = styled.div`
  *:not(path):not(g) {
    color:       hsla(210, 100%, 100%, 0.9) !important;
    background:  hsla(210, 100%, 50%, 0.5) !important;
    outline:     solid 0.25rem hsla(210, 100%, 100%, 0.5) !important;
  
    box-shadow: none !important;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  font-family: sans-serif;
`;


const stickyTop = {
  position: "fixed",
  display: "block",
  zIndex: 99999,
    color:      "hsla(210, 100%, 100%, 0.9) ",
    background: "rgba(246, 249, 252, 0.6) ",
    outline:     "solid 0.25rem hsla(210, 100%, 100%, 0.5)",
};


/* <JsonView src={{...props} } compass="SW" name="WORKING"/> */


export const Line = (props) => (
	<div class="is-divider"
	     data-content="- "
  	     style={{margin: "1%", borderTop: ".1rem solid #dbdbdb"}}
	/>
);

export const Divider = (props) => (
	<div class="is-divider"
	     data-content="- "
  	     style={{margin: "5%", borderTop: ".1rem solid #dbdbdb"}}
	/>
);

export const JsonView =  (props) => {
  var style = {...stickyTop};
  switch(props.compass) {
    case "NW": 
	style.top = 0; 
	style.left = 0;
	break;
    case "NE":
	style.top = 0;
	style.right = 0;
	break;
    case "SE":
	style.bottom = 0;
	style.right = 0;
	break;
    case "SW":
	style.bottom = 0;
	style.left = 0;
	break;
    default: 
	style.top = 0;
	style.right = 0;
  }
  if(props.embed) {
	style={};
  }

 return (
   <div style={style} >
    <ReactJson name={props.name} src={{...props.src}} />
    </div>
 );
};

export const JsonPre = (props) => (
	<>
    <pre>{JSON.stringify(props.src,
	    (key, value) => {
		if(value === undefined) {
		  return "";
		}
		return value;
	    }, 2)}</pre>
	</>
);

export const Lab = (props) => (
	<div className="box" style={{ padding: "0px" }} >
  {props.children}
	</div> 
);
