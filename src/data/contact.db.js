import Noa from  '../Noa';

import NoaMedia from '../NoaMedia';

import { weHelpSection, contactInfo, servicesSection } from './commons.db';

export const howGoodSection = {
			title: "Que bien que estes aca !",
			subtitle: "Siempre estamos dispuestos a asumir nuevos retos. Contanos el tuyo.",
			legend: "",
			action: {
				label: "HABLEMOS",
				submitHandler: (e) => { console.log("We help requested"); }
			},
			css: { 
				heroContainer: {
					background: `linear-gradient(to top, rgba(50, 115, 220, 0.25) 100%, transparent), url(${NoaMedia.company.team._images[1].picUri}) no-repeat center center`, 
					backgroundSize: "cover"
				},
				actionStyle: {
					color: "#23d160",
					borderColor: "#23d160",
					backgroundColor: "transparent"
				},
				actionOnOverStyle: { }
			}
};




export const contactForm = {
  origin: "",
  locale: "es_AR",
  form: {
	  legend: "Dejanos un mensaje y nos pondremos en contacto cuanto antes.",
	  fields: {
	    ContactName: { label: "Como te llamas ?" },
	    ContactEmail: { label: "Tu email" },
	    ContactPhone: { label: "Un telefono de contacto" },
	    ContactCompany: { label: "De que empresa sos?" },
	    Message: { label: "En que podemos ayudarte ?" },
	    Send: { label: "Enviar mensaje" }
	  }
  }
};

export const contactPage = {
sections: {
	mainHero: howGoodSection,
	contactForm: contactForm,
	contactInfo: contactInfo,
	contactMap: contactInfo["gis-data"],
	services: servicesSection
}
};

export default contactPage;

