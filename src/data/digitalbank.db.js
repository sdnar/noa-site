import Noa from  '../Noa';
import NoaMedia from '../NoaMedia';
import { MEDIA_DIGITAL_BANKING } from '../NoaMedia';

/** COLOR GRADIENTE DE EJEMPLO 
    themeColor: "linear-gradient(340deg, rgba(111,44,172,0.35), rgba(11,44,172,0.85))",
 **/
export const digitalbankPage = {
sections: {
	digitalRevolution: {
	  title: "Impulsando la <strong> revolucion de la banca digital.</strong>",
	  subtitle: "",
	 legend: "La industria bancaria ha cambiado. Ayudamos a los bancos a cambiar a traves de una Plataforma Integradora Digital.",
	 background: {
	   image: NoaMedia.company._images[1],
	   color: ""
	 },
	},
	digitalWin: {
		title: "La <strong>Banca Digital</strong> es ahora.",
	  subtitle: "",
	 legend: "Los bancos digitales y las nuevas tecnologias estan cambiando la industria finaciera. Ellos ganan por que innovan rapidamente y ofrecen todos sus servicios mediante una experiencia simple y amigable. En Soft Del Noa te ayudamos a operar de la misma manera y recuperar esta ventaja competitiva.",
	 background: {
	   image: NoaMedia.company._images[1],
	   color: ""
	 },

	},
	toOmnichanel: {},
	beAgile: {},
	digitalExperience: {
		title: "Casos de Exito",
		subtitle: "Desde nuestros inicios, hemos consolidado una solida experiencia nacional e internacional en proyectos de innovacion para la industria digital financiera.",
		legend: "Nos vamos cuando esta terminado. Este compromiso para el trabajo ha consolidado la confianza de nuestros clientes en todo el equipo noa.",
		projects: [
		{ title: "Integracion Interbaking DataNet",
			overview: "Conector online/ofline para los servicios de Banca Electronica Multibanco.", 
		  client: "Banco Piano / Interbaking",
		  clientLogo: `${MEDIA_DIGITAL_BANKING}/logo_bancopiano.png`, 
	          themeColor: "rgba(111,44,172,1)",
		  image: `${MEDIA_DIGITAL_BANKING}/interbanking.png`
		},

		{ title: "Monitoreo Renaper",
		  overview: "Controlador de fallecimiento para beneficiarios sociales mediante consultas online al Registro Nacional.",
	          client: "BANCO PIANO / INTERBAKING",
		  clientLogo: `${MEDIA_DIGITAL_BANKING}/logo_bancopiano.png`, 
	          themeColor: "rgba(0, 121, 255)",
		  image: `${MEDIA_DIGITAL_BANKING}/renaper_02.png`
		},
		{ title: "Debito Inmediato Link",
		  overview: "Motor para la creacion de Plazos Fijos Web mediante conexion a servicios de Debitos y Creditos inmediatos interbancarios.",
		  client: "Banco Piano",
		  clientLogo: `${MEDIA_DIGITAL_BANKING}/logo_bancopiano.png`, 
	          themeColor: "rgba(18, 159, 0)",
		  image: `${MEDIA_DIGITAL_BANKING}/debines_02.jpg`
		},
		]
	},
	servicesSection: {
		title: "PASION, METODOLOGIA y TALENTO",
		subtitle: "Personas que entienden, software que sirve",
		services: [
			{...Noa.services.soporte, media: NoaMedia.company.services.soporte},
			{...Noa.services.mobile, media: NoaMedia.company.services.mobile},
			{...Noa.services.agile, media: NoaMedia.company.services.agile},
			{...Noa.services.software, media: NoaMedia.company.services.software},
			{...Noa.services.auditoria, media: NoaMedia.company.services.auditoria},
			{...Noa.services.consultoria, media: NoaMedia.company.services.consultoria},
		]

	},
	wehelpSection: {
		title: "Necesitas ayuda con tu proyecto ?",
		subtitle: "Contactanos y has crecer tu negocio hoy.",
		legend: "Velocidad para el cambio.",
		action: {
			label: "HABLEMOS",
			submitHandler: (e) => { console.log("We help requested"); }
		},
		css: { 
			heroContainer: {
				background: `linear-gradient(to top, rgba(50, 115, 220, 0.25) 100%, transparent), url(${NoaMedia.company.team._images[1].picUri}) no-repeat center center`, 
				backgroundSize: "cover"
			},
			actionStyle: {
				color: "#23d160",
				borderColor: "#23d160",
				backgroundColor: "transparent"
			},
			actionOnOverStyle: { }
		}
	}

}
}

export default digitalbankPage;
