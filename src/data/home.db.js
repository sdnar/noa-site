import Noa from  '../Noa';
import NoaMedia from '../NoaMedia';

import { weHelpSection, servicesSection } from './commons.db';

export const homePage = {
	sections: {
		workingSection: {
			title: "APLICAR LO NUEVO AHORA.",
			subtitle: "Nuevas ideas. Resultados tangibles.",
			legend: "Expertos en desarrollo de productos digitales.",
			_meta: {
				backgroundImage: NoaMedia.company.team._images[0],
				contactBtn: "HABLA CON UN CONSULTOR"
			}
		},
		productsSection: {
			title: "DESARROLLOS SOFT DEL NOA.",
			subtitle: "Personas que entienden, software que sirve",
			legend: `Tenemos mas herramientas para la transformacion digital <a href="#"><span className="innerlink"> conocelas todas</span></a>`,
			mainProduct: Noa.products.dflow,
			otherProducts: [Noa.products.dflow, Noa.products.durban],
		},
		clientsSection: {
			title: "ALGUNOS DE NUESTROS CLIENTES.",
			subtitle: "A quien ayudamos en su transformacion digital ?",
			legend: `Hemos transformado digitalmente a mas de 20 empresas.`,
			clients: Object.entries(Noa.clientes)
			.map(data => data[1])
			.map(client => { return { name: client.name, logo: client.media.logos[0], css: client.media.logos[0].css }; })
		},
		servicesSection: servicesSection,
		wehelpSection: weHelpSection
	}
};

export default homePage;
