import Noa from  '../Noa';

import NoaMedia from '../NoaMedia';

export const servicesSection =	{
			title: "PASION, METODOLOGIA y TALENTO",
			subtitle: "Personas que entienden, software que sirve",
			services: [
				{...Noa.services.soporte, media: NoaMedia.company.services.soporte},
				{...Noa.services.mobile, media: NoaMedia.company.services.mobile},
				{...Noa.services.agile, media: NoaMedia.company.services.agile},
				{...Noa.services.software, media: NoaMedia.company.services.software},
				{...Noa.services.auditoria, media: NoaMedia.company.services.auditoria},
				{...Noa.services.consultoria, media: NoaMedia.company.services.consultoria},
			]

};

export const weHelpSection = {
			title: "Necesitas ayuda con tu proyecto ?",
			subtitle: "Contactanos y has crecer tu negocio hoy.",
			legend: "Velocidad para el cambio.",
			action: {
				label: "HABLEMOS",
				submitHandler: (e) => { console.log("We help requested"); }
			},
			css: { 
				heroContainer: {
					background: `linear-gradient(to top, rgba(50, 115, 220, 0.25) 100%, transparent), url(${NoaMedia.company.team._images[1].picUri}) no-repeat center center`, 
					backgroundSize: "cover"
				},
				actionStyle: {
					color: "#23d160",
					borderColor: "#23d160",
					backgroundColor: "transparent"
				},
				actionOnOverStyle: { }
			}
};


export const contactInfo = {
				"company": "Soft del NOA",
				"reference": " ",
				"address": [
					"Av Libertador 1, piso 13",
				   "Las Canitas ",
				   "cp 1833 Capital Federal",
				   "Buenos Aires, Argentina"
				],
				"phones": [
					"(+54) 011 155011030",
					"info@softdelnoa.com"
				],
				"gis-data": {
				"google-map": {
src:"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2323.313103670728!2d-58.44570481488797!3d-34.56214020035283!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcb5c9134a4611%3A0x1005a26e58570e1d!2sAv.%20del%20Libertador%205480%2C%20Buenos%20Aires!5e0!3m2!1ses!2sar!4v1572466170931!5m2!1ses!2sar", width:"100%", height:"450" },
				   "point": { latitude: "", longitude: ""}
				} 
};

export default { weHelpSection, servicesSection, contactInfo };
