import Noa from  '../Noa';

import NoaMedia from '../NoaMedia';

export const consultoriaPage = {
sections: {
	wheAreSection: {
		slides: [
			{ title: "Consultoria SIU",
				subtitle: "Asesoriamiento y Consultoria SIU de Alto Nivel",
				backgroundImage: NoaMedia.company.services.consultoria.images[0],
				css: {
					hero: {},
					title: { color: "white", fontHeight: "1", fontSize: "35px", fontWeight: "400", lineHeight: "1", textShadow: "1px 1px #777" },
					subtitle: { color: "white", fontWeight: "400" }
				}
			},

			{ title: "Cursos SIU",
				subtitle: "Disenados para adquirir excelencia en SIU y sus principales modulos.",
				backgroundImage: NoaMedia.company.services.consultoria.images[2],
				css: {
					hero: {},
					title: { color: "white", fontHeight: "1", fontSize: "35px", fontWeight: "400", lineHeight: "1", textShadow: "1px 1px #777" },
					subtitle: { color: "white", fontWeight: "400" }
				}
			},


			{ title: "SIU <br/> para su Institucion",
				subtitle: "Evaluamos, Implementamos, Recuperamos y Optimizamos SIU en su Institucion.",
				backgroundImage: NoaMedia.company.services.consultoria.images[1],
				css: {
					hero: {},
					title: { color: "white", fontHeight: "1", fontSize: "35px", fontWeight: "400", lineHeight: "1", textShadow: "1px 1px #777" },
					subtitle: { color: "white", fontWeight: "400" }
				}
			},


			{ title: "Gestion de <br/> Talento",
				subtitle: "Encontramos los Profesionales SIU mas cualificados para su Institucion",
				backgroundImage: NoaMedia.company.services.consultoria.images[3],
				css: {
					hero: {},
					title: { color: "white", fontHeight: "1", fontSize: "35px", fontWeight: "400", lineHeight: "1", textShadow: "1px 1px #777" },
					subtitle: { color: "white", fontWeight: "400" }
				}
			},

		]
	},
	siuConsultantsSection:  {
		phrases: [
			{ phrase: "Somos una Consultora Tecnologica especializada en ofrecer servicios relacionados con soluciones SIU. Desde la implementacion del Sistema a realizar formacion especifica a su plantilla en los diferentes ambitos, estadistico, academico entre otros.", author: "",
				css: { phraseStyle: { color: "#777", fontSize: "32px", fontWeight: "400", lineHeight: "1.2", margin: "0 10%"} } },
		]
	},
	siuServicesSection: {
		title: "SERVICIOS SIU",

		subtitle: "Como socio Tecnologico, disponemos de los ultimos recursos humanos y tecnologicos para darle solucion mas adecuada a sus necesidades.",
		services: [ 
			{...Noa.services.consultoria.services[0], media: NoaMedia.company.services.consultoria.services2[0] },
			{...Noa.services.consultoria.services[1], media: NoaMedia.company.services.consultoria.services2[1] },
			{...Noa.services.consultoria.services[2], media: NoaMedia.company.services.consultoria.services2[2] },
			{...Noa.services.consultoria.services[3], media: NoaMedia.company.services.consultoria.services2[3] }
		],

		css: {
			itemBoxClasses: [ "is-one-quarter" ],
			sloganStyle: { textAlign: "center" },
			titleStyle: { fontSize: "32px", fontWeight: "400", color: "#2e3541", lineHeight: "1.2"},
			subtitleStyle: { width: "75%", paddingLeft: "0", margin: "0 auto 40px", textAlign: "center", color: "#777", fontSize: "1.4rem", fontWeight: "400" }
		}
	},
	whoIsSiu: {
		title: "Soluciones SIU",
		subtitle: "En Soft del NOA Confiamos en SIU como Sistema de Informacion Universitario para pequenas y medianas instituciones, ya que SIU es la Solucion que mas control y escalabilidad le va a ofrecer de cuantos hay en el mercado, junto con la robustez de un producto 100% nacional. ",
		othertitle: "TECNOLOGIAS",
		products: [
			{ title: "Que es SIU ?",
				overview: "La solucion SIU es un Sistema de Informacion Universitario enfocado a la pequena y mediana institucione academica.", media: NoaMedia.company.services.consultoria.services3[0] } ,
			{ title: "SIU-Guarani", overview: "Modulo de Gestion Academica, registra las activades dentro de la universidad desde que un alumno se inscribe hasta que egresa.", media: NoaMedia.company.services.consultoria.services3[1] } ,
			{ title: "SIU-Araucano", overview: "Modulo de Estadisticas de Alumnos, contiene la informacion estadistica de carreras de pregrado, grado y postgrado. Ademas procesa las cifras de oferta eductativa.", media:NoaMedia.company.services.consultoria.services3[2], 
				css: { paddingBottom: "17px", backgroundColor: "lightgray"} } ,
			{ title: "SIU-Toba", overview: "Ambiente de Desarrollo Web, es una herramienta de desarrollo que permite crear sistemas transaccionales en forma rapida, utilizando tecnologia web open-source.", media: NoaMedia.company.services.consultoria.services3[3], 
				css: { paddingBottom: "17px", backgroundColor: "rgba(106,74,60,1)"} } ,
		],
		css: {
			itemBoxClasses: [ "is-one-quarter" ],
			sloganStyle: { textAlign: "center" },
			titleStyle: { fontSize: "32px", fontWeight: "400", color: "#2e3541", lineHeight: "1.2", textAlign: "center"},
			subtitleStyle: { width: "85%", paddingLeft: "0", margin: "0 auto 40px", textAlign: "center", color: "#777", fontSize: "1.2rem", fontWeight: "400" }
		}
	},
	aboutUS: {
		title: "SOBRE NOSOTROS",
		overview: "Somos Soft del NOA, una Consultora Especializada en SIU que Cree en el Talento para conseguir resultados Empresariales Optimos. Estamos formados por un equipo de Profesionales Altamente Cualificados para Analizar, Evaluar y Implementar SIU. Utilizamos SIU para conseguir resultados sorprendentes. Aunamos Soluciones Estrategicas IT con Gestion de Talento. Con nuestra ayuda podra crear una base solidad de Gestion SIU para su institucion educativa con la que controlar y mejorar sus resultados.",
		services: [ {...Noa.services.consultoria.services[0], icon: "fab fa-patreon"},
			{...Noa.services.consultoria.services[1], icon: "fas fa-rss"},
			{...Noa.services.consultoria.services[2], icon: "fas fa-plug"},
			{...Noa.services.consultoria.services[2], icon: "fas fa-rocket"} ],
		css: {},
		owner: "Ing. Sandra Giobellina"
	},
	wehelpSection: {
		title: "Solicite una Cita",
		subtitle: "Hablemos de su Proyecto.",
		legend: "",
		action: {
			label: "Adelante!",
			submitHandler: (e) => { console.log("We help requested"); }
		},
		css: { 
			heroContainer: {
				backgroundImage: NoaMedia.company.team._images[1],
				background: `url(${NoaMedia.company.team._images[1].picUri}) no-repeat center center`, 
				backgroundSize: "cover"
			},
			actionStyle: {
				color: "#23d160",
				borderColor: "#23d160",
				backgroundColor: "transparent"
			},
			actionOnOverStyle: { }
		}
	},
	clientsSection: {
		title: "NUESTROS CLIENTES Y PARTNERS",
		subtitle: "Constamos con amplia experincia en el sector educativo argentino.",
		legend: "",
		clients: Object.entries({una: Noa.clientes.una, fiuba: Noa.clientes.fiuba, fadu: Noa.clientes.fadu, pna: Noa.clientes.pna, inun: Noa.clientes.inun, unla: Noa.clientes.unla,
			usam:  Noa.clientes.unsam, undec: Noa.clientes.undec, pfa: Noa.clientes.pfa })
		.map(data => data[1])
		.map(client => { console.log(client); return { name: client.name, logo: client.media.logos[0], css: client.media.logos[0].css }; })

	}
}
};

export default consultoriaPage;
