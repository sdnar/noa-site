import Noa from '../Noa';

import NoaMedia from '../NoaMedia'; 
import { MEDIA_SERVICES } from '../NoaMedia'; 


export const servicesPage = {
  pick: {
	  icon: `${MEDIA_SERVICES}/software_develop_45.jpg`,
	  title: "Expertos en desarrollo de productos digitales",
	  description: "Transformamos problemas complejos en soluciones de futuro acompanando a nuestros clientes."
  },
  stats:[
	{ name: "Proyecto", count: 40, message: "Adaptandonos al mercado digital, desde la banca digital hasta plataformas educativas." , more: 0  },	
	{ name: "Cliente", count: 20, message: "Acompanandolos con soluciones digitales en sus proyectos mas estrategicos.", more: 0   },
	{ name: "Horas Desarrollo", count: 1000, message: "Un Millar de Horas desarrollando soluciones de futuro, siendo referentes en eficacia y buenas practicas.", more: 1   }
  ]
};



export default servicesPage;
