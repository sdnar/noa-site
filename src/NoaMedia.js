export const CDN_HOST = "http://192.168.1.65:8080";
export const MEDIA = `${CDN_HOST}/media`;
export const MEDIA_PRODUCTS = `${CDN_HOST}/media/products`;
export const MEDIA_CLIENTS = `${CDN_HOST}/media/company/clients`;
export const MEDIA_SERVICES = `${CDN_HOST}/media/company/services`;
export const MEDIA_DURBAN = `${CDN_HOST}/media/company/products/durban`;
export const MEDIA_DFLOW = `${CDN_HOST}/media/company/products/dflow`;
export const MEDIA_TEAM = `${CDN_HOST}/media/company/team`;
export const MEDIA_COMPANY = `${CDN_HOST}/media/company`;

export const MEDIA_DIGITAL_BANKING = `${CDN_HOST}/media/company/services/banking`;

const NoaMedia = {
  company: {
   _logos: [
	   { picUri: `${MEDIA_COMPANY}/logos/logo_01.png` }
   ],
   _videos: [
   ],
   _images: [
	   { picUri: `${MEDIA_COMPANY}/horizon_01.jpg` },
	   { picUri: `${MEDIA_COMPANY}/horizon_02.jpg` }
   ],
  team: {
    _videos: [],
    _images: [
    { picUri: `${MEDIA_TEAM}/images/team_01.jpg` },
    { picUri: `${MEDIA_TEAM}/images/team_02.jpg` }
    ],
    _avatars: [] 
  },
     clients: { 
	     arsat         : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/arsat_01.png`, css: { backgroundColor: "#f14668", padding: "10px" }} ], images: [], videos: [] },
	     modernizacion : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/modernizacion_01.png?1=1`, css: {  }} ], images: [], videos: [] },
	     piano         : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/piano_01.png`, css: { backgroundColor: "#b5b5b5", padding: "10px", boxShadow: "14px" } } ], images: [], videos: [] },
	     siu           : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/siu_01.png`} ], images: [], videos: [] },
	     caba          : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/caba_01.png`, css: { padding: "10px", backgroundColor: "#fdd234"}} ],  images: [], videos: [] },
	     una           : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/una_01.png`, css: { backgroundColor: "white", padding: "5px", boxShadow: "2px" }}  ] },
	     fiuba         : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/fiuba_01.png`, css: { backgroundColor: "#f14668", padding: "5px" }} ] },
	     fadu          : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/fadu_01.svg`} ] },
	     pna           : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/pna_01.png`} ] },
	     inun          : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/inun_01.gif`} ] },
	     hibu          : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/hibu_01.png`} ] },
	     guru          : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/guru_01.png`} ] },
	     unla          : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/unla_01.jpg`} ] },
	     unsam         : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/unsam_01.png`} ] },
	     pfa           : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/pfa_01.jpg`} ] },
	     undec 	    : { logos : [ {picUri: `${MEDIA_CLIENTS}/logos/undec_01.png`, css: { backgroundColor: "#b5b5b5", padding: "5px" }} ] }
   },
   partners: {
      _all: { logos: [], images:[], videos: [] },
      siu: { logos: [], images: [], videos: [] }
   },
  products: {
     dflow: {
	     logos: [],
	     videos: [
		     { tubeKey: "QgGbMTO_qfw" }
	     ],
	     images: [ { picUri: `${MEDIA_DFLOW}/images/deck/deck_01.png`} ]
     },
     durban: { logos: [], videos: [] ,
	     images: [ { picUri:  `${MEDIA_DURBAN}/images/deck/deck_01.png`} ]
     }
  },
  services: { 
     soporte  : { logos : [ {picUri: `${MEDIA_SERVICES}/logos/soporte_01.svg`, css: { }} ], images: [], videos: [] },
     mobile  : { logos : [ {picUri: `${MEDIA_SERVICES}/logos/mobile_01.svg`, css: { }} ], images: [], videos: [] },
     disenio  : { logos : [ {picUri: `${MEDIA_SERVICES}/logos/disenio_01.svg`, css: { }} ], images: [], videos: [] },
     agile  : { logos : [ {picUri: `${MEDIA_SERVICES}/logos/agile_01.svg`, css: { }} ], images: [], videos: [] },
     software  : { logos : [ {picUri: `${MEDIA_SERVICES}/logos/software_01.svg`, css: { }} ], images: [], videos: [] },
     auditoria  :   { logos : [ {picUri: `${MEDIA_SERVICES}/logos/auditoria_01.svg`, css: { }} ], images: [], videos: [] },
     consultoria  : { 
	   logos : [ {picUri: `${MEDIA_SERVICES}/logos/consultoria_01.svg`, css: { }} ],
	   images: [ 
		   {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_1.jpg`, css: {color: "white !important"}} ,
		   {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_2.jpg`, css: {color: "white !important"}} ,
		   {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_3.jpg`, css: {color: "white !important"}} ,
		   {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_4.jpg`, css: {color: "white !important"}} ,
	   ],
	   videos: [],
	   services: [
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_1.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_2.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_3.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_4.jpg`, css: { }} ], images: [], videos: [] },
	   ],
	   services2: [
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_1_box.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_2_box.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_3_box.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_4_box.jpg`, css: { }} ], images: [], videos: [] },
	   ],
	   services3: [
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_6.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_11.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_10.jpg`, css: { }} ], images: [], videos: [] },
		   { logos : [ {picUri: `${MEDIA_SERVICES}/consultoria/images/consultoria_promo_5.jpg`, css: { }} ], images: [], videos: [] },
	   ]
     }
  }
 }
};

export default NoaMedia;
