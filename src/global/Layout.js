import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
// import { StaticQuery, graphql } from 'gatsby';

import Header from '../components/Header';
import Footer from '../components/Footer';

import  Noa  from '../Noa';
import NoaSite from '../NoaSite';
import { headerLinks, footerLinks  } from '../NoaSite';

import GlobalStyle from './GlobalStyle';
const klobal= {
	   headerLinks: headerLinks,
	   footerLinks: footerLinks,
	   footerMark: { name: Noa.name },
	   footerYear: { year: "2019" }
	};

const company = { name: klobal.footerMark.name };
const year =  klobal.footerYear;

export const  LayoutLite = (props) => (
      <>
        <Helmet
          title={Noa.title}
		meta={[
		{ name:"author",  content: Noa.author },
		{ name:"description",  content: Noa.description },
		{ name:"keywords",  content: Noa.keywords },
		{ name:"wiewport",  content: "width=device-width, initial-scale=1.0"}
		]}
   	>
   	</Helmet>
        <GlobalStyle />
	<Header links={headerLinks} company={company}  />
        {props.children}
	<section>
		<Footer links={footerLinks} company={company} year={year} />
	</section>
      </>
);

LayoutLite.propTypes = {
  children: PropTypes.node.isRequired,
};



export const  Layout = (props) => (
      <>
        <Helmet
          title={Noa.title}
		meta={[
		{ name:"author",  content: Noa.author },
		{ name:"description",  content: Noa.description },
		{ name:"keywords",  content: Noa.keywords },
		{ name:"wiewport",  content: "width=device-width, initial-scale=1.0"}
		]}
   	>
   	</Helmet>
        <GlobalStyle />
	<section className="hero is-fullheight is-light" >
	<Header links={headerLinks} company={company}  />
        {props.children}
	</section>
	<section>
		<Footer links={footerLinks} company={company} year={year} />
	</section>
      </>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
