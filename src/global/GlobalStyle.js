import { createGlobalStyle } from 'styled-components';
import reset from 'styled-reset-advanced';

import '../components/v2/styles/general.module.css';
import '../components/v2/styles/content.module.css';
import '../components/v2/styles/block.module.css';
import '../components/v2/styles/block.image.module.css';

import '../assets/bulma/css/bulma.css';
import '../assets/bulma/css/main.css';
import '../assets/bulma/css/mixins.css';
import '../assets/css/main.css';
// import '../assets/bulma/css/debug.css';

import '../styles/v2/global.css';
import '../styles/v2/digitalbank_home.css';
import '../styles/v2/components__hero.css';
import '../styles/v2/components__stickhero.css';
import '../styles/v2/components__features--three.css';

export default createGlobalStyle`
  ${reset};

  body {
    font-family: 'Avenir Next', 'Helvetica Neue', 'Helvetica', sans-serif;
    font-weight: 500;
  }
`;
