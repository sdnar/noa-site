import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { MEDIA_SERVICES } from '../NoaMedia'; 


import { JsonView  } from '../styles';
import { LayoutLite } from '../global/Layout';
import ServiceScreen from '../components/screens/ServiceScreen';

import { services  } from '../data/services.db';


export const db = {
  pick: {
	  icon: `${MEDIA_SERVICES}/software_develop_45.jpg`,
	  title: "Expertos en desarrollo de productos digitales",
	  description: "Transformamos problemas complejos en soluciones de futuro acompanando a nuestros clientes."
  },
  stats:[
	{ name: "Proyecto", count: 40, message: "Adaptandonos al mercado digital, desde la banca digital hasta plataformas educativas." , more: 0  },	
	{ name: "Cliente", count: 20, message: "Acompanandolos con soluciones digitales en sus proyectos mas estrategicos.", more: 0   },
	{ name: "Horas", count: "1M", message: "Un Millar de Horas desarrollando soluciones de futuro, siendo referentes en eficacia y buenas practicas.", more: 1   }
  ],
  services: [
		{ title: "Software Factory",
			overview: "Pasion, Metodologia y Talento para el Disenio, Desarrollo, Soporte de Soluciones Digitales Multicanal.", 
		  client: "Banco Piano / Arsat",
		  clientLogo: `${MEDIA_SERVICES}/logo_bancopiano.png`, 
	          themeColor: "rgba(111,44,172,1)",
		  image: `${MEDIA_SERVICES}/software_develop_10.jpg`,
		  slug: '/'
		},

		{ title: "Banca Digital",
		  overview: "Controlador de fallecimiento para beneficiarios sociales mediante consultas online al Registro Nacional.",
	          client: "BANCO PIANO",
		  clientLogo: `${MEDIA_SERVICES}/logo_bancopiano.png`, 
	          themeColor: "rgba(0, 121, 255)",
		  image: `${MEDIA_SERVICES}/software_develop_12.jpg`,
		  slug: '/digitalbank-home'
		},
		{ title: "Consultoria SIU",
		  overview: "Consultoria especializada en ofrecer servicios relaciones con soluciones SIU. Desde la implementacion a la formacion.",
			client: "CUBA / UBA / UNLA / UFA / CHAU",
		  clientLogo: `${MEDIA_SERVICES}/logo_bancopiano.png`, 
	          themeColor: "rgba(18, 159, 0)",
		  image: `${MEDIA_SERVICES}/software_develop_09.jpg`,
		  slug: '/consultoria'
		},
   ]
};

  
export const ServicesPage = ({data}) => {

return (
  <LayoutLite>
	<ServiceScreen {...{data: data}} />
  </LayoutLite>
);

}


export const DefaultServicesPage = (props) => {
return (
   <ServicesPage {...{data: db}}/>
);
}

export default DefaultServicesPage;
