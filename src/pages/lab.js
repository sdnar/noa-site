import React from 'react';
import { useEffect, useState } from 'react';
import { useRef } from 'react'

import styled from 'styled-components';

import { css } from '@emotion/core';

import { LayoutLite } from '../global/Layout';

import {Line, Divider, Debug  } from '../styles';

import AboutUSWidget from '../fragments/AboutUS';

import Noa from '../Noa';
import NoaSite from '../NoaSite';

export const LabPage = (props) => {

const _consultoriaPage = NoaSite.pages.consultoria;
const _aboutUS = _consultoriaPage.sections.aboutUS;

return (
<LayoutLite>
<AboutUSWidget
    css={_aboutUS.css}
    title={_aboutUS.title}
    overview={_aboutUS.overview}
    services={_aboutUS.services} /> <Divider/>

</LayoutLite>
);
};

export default LabPage;
