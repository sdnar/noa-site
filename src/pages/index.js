import React from 'react';
import styled from 'styled-components';

import { css } from '@emotion/core';

import Layout from '../global/Layout';

import { Debug, JsonView, JsonPre } from '../styles';

import WorkingBanner from '../fragments/WorkingBanner';
import ProductSection from '../fragments/ProductSection';
import ListOfClientsWidget from '../fragments/ListOfClientsWidget';
import ListOfServicesWidget from '../fragments/ListOfServicesWidget';
import WeHelpYouWidget from '../fragments/WeHelpYouWidget';

import { getMedia } from '../Utils';

import Noa from '../Noa';
import NoaSite from '../NoaSite';

const _meta = NoaSite.pages.home;
const _working = NoaSite.pages.home.sections.workingSection;
const _products = NoaSite.pages.home.sections.productsSection;
const _clients = NoaSite.pages.home.sections.clientsSection;
const _services = NoaSite.pages.home.sections.servicesSection;
const _wehelp = NoaSite.pages.home.sections.wehelpSection;

const IndexPage = () => { console.log(_services);
return (
  <Layout>
     <WorkingBanner
	 backgroundImage={_working._meta.backgroundImage}
	 title={_working.title}
	 subtitle={_working.subtitle}
	 legend={_working.legend}
	 contactBtn={_working._meta.contactBtn}
     />


     <div class="is-divider" data-content="- " style={{maring: "5%", borderTop: ".1rem solid #dbdbdb"}} />

     <ProductSection
	     title={_products.title}
	     subtitle={_products.subtitle}
	     legend={_products.legend}
	     mainProduct={_products.mainProduct}
	     otherProducts={_products.otherProducts}
     />

     <div class="is-divider" data-content="- " style={{margin: "5%", borderTop: ".1rem solid #dbdbdb"}} />
    
    <div style={{paddingLeft: "15%", paddingRight: "15%"}}>
     <ListOfClientsWidget
	     title={_clients.title}
	     subtitle={_clients.subtitle}
	     legend={_clients.legend}
	     clients={_clients.clients}
     />
     </div>

     <div class="is-divider" data-content="- " style={{margin: "5%", borderTop: ".1rem solid #dbdbdb"}} />

     <div style={{alignItems: "center", textAlign: "center" }}>
     <ListOfServicesWidget 
	     title={_services.title}
	     subtitle={_services.subtitle}
	     legend={_services.legend}
	     services={_services.services}
     /> </div>

     

     <div style={{alignItems: "center", textAlign: "center" } }  
	     class="column hero is-height  is-fullwidth ">
     <WeHelpYouWidget 
	     class="is-height"
	     title={_wehelp.title}
	     subtitle={_wehelp.subtitle}
	     legend={_wehelp.legend}
	     css={_wehelp.css}
	     accBtn={_wehelp.action}
     /> </div>


     <div class="is-divider" data-content="- " style={{margin: "5%", borderTop: ".1rem solid #dbdbdb"}} />

  </Layout>
); }

/*  maxWidth: "66rem" */

export default IndexPage;
