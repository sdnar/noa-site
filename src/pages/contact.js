import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { LayoutLite } from '../global/Layout';
import ListOfServicesWidget from '../fragments/ListOfServicesWidget';
import WeHelpYouWidget from '../fragments/WeHelpYouWidget';

import ContactScreen from '../v3/components/screens/ContactScreen';

import {GuideWrapper} from '../v3/components/screens/IndexScreen/Guide';

import { Debug, JsonView, JsonPre  } from '../styles';
import { Line, Divider  } from '../styles';

import NoaSite from '../NoaSite';


const page = NoaSite.pages.contact;

const ContactPage = props => (
<LayoutLite>
				<ContactScreen {...NoaSite.pages.contact.sections}/>
</LayoutLite>
);

export default ContactPage;
