import React from 'react';

import NoaSite from '../NoaSite';

import { LayoutLite } from '../global/Layout';

import { SloganBanner  } from '../components/v2/SloganBanner';

import { ListOfItems } from '../components/v2/FeatureList';

import { Debug, JsonView, JsonPre, Line } from '../styles';
import ListOfServicesWidget from '../fragments/ListOfServicesWidget';
import WeHelpYouWidget from '../fragments/WeHelpYouWidget';

import { Hero, StickHero } from '../components/v2';

import Guides from '../v3/components/screens/IndexScreen/Guides';
import Pitch from '../v3/components/screens/IndexScreen/Pitch';

const _services = NoaSite.pages.digitalbank.sections.servicesSection;
const _wehelp = NoaSite.pages.digitalbank.sections.wehelpSection;
const _sections = NoaSite.pages.digitalbank.sections;

const DigitalRevolution   = ({ data }) => (
				<Hero {...data} />
);

const DigitalWin          = ({ data }) => ( 
				<StickHero {...data} />	
);

const ToOmmiChannel           = (props) => ( <>
</> );

const BeAgile                 = (props) => ( <></> );

const DigitalExperience   = ({data}) => {

const nodes = data.projects.map((project) => {
	return {
		node: {
			fields: {
				guide: '#',
				slug: '#'
			},
			frontmatter: {
				description: project.overview,
				title: project.title,
				chapter: project.client,
				themeColor: project.themeColor,
				thumbImagePath: project.image
			}
		}
	};
});

const guidesProps = {
  chaptersEdges: [
    {
      node: {
        fields: {
          guide: 'guide',
        },
      },
    },
  ],
  guidesEdges: nodes,
};

return (
<section>
  <div class="v3">
  <Pitch  title={data.title} subtitle={data.subtitle} / >
  </div>
  <Guides {...guidesProps}/>}

</section>
);
}


const DigitalExperience1   = ({data}) => (
	<div class="is-light" >
	<SloganBanner title={data.title} subtitle={data.subtitle} legend={data.legend} >
		<ListOfItems items={data.projects} css={{}} />
	</SloganBanner> 
	</div>);


const DigitalBankHome = (props) => {

return (
  <LayoutLite>
	<div class="v2">
	  <DigitalRevolution data={_sections.digitalRevolution}/>
	  <DigitalWin data={_sections.digitalWin}/>
	  <ToOmmiChannel data={_sections.toOmmiChannel}/>
	  <BeAgile data={_sections.beAgile}/>
	  <DigitalExperience data={_sections.digitalExperience}/>
	</div>
     <div class="is-divider" data-content="- " style={{margin: "5%", borderTop: ".1rem solid #dbdbdb"}} />

     <div style={{alignItems: "center", textAlign: "center" }}>

     <ListOfServicesWidget 
	     title={_services.title}
	     subtitle={_services.subtitle}
	     legend={_services.legend}
	     services={_services.services}
     /> </div>

     <div style={{alignItems: "center", textAlign: "center" } }  
	     class="column hero is-height  is-fullwidth ">
     <WeHelpYouWidget 
	     class="is-height"
	     title={_wehelp.title}
	     subtitle={_wehelp.subtitle}
	     legend={_wehelp.legend}
	     css={_wehelp.css}
	     accBtn={_wehelp.action}
     /> </div>

     <Line/>
  </LayoutLite>
)
};

export default DigitalBankHome;
