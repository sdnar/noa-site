import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';



export const Box = styled.span`
  display: block;
	background: ${props => props.themeColor};
  color: #fff;
  padding: 40px;
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  text-align: left;
  height: 400px;

`;



export const Colors = ({mainHero, contactForm, services}) => (
				<>
			<>
				<Box themeColor="indigo">INDIGO</Box>
				<Box themeColor="purple">PURPLE</Box>
			</>
			<>
				<Box themeColor="red">RED</Box>
				<Box themeColor="#da4334">RED_2</Box>
			</>
			<>
				<Box themeColor="orange">ORANGE</Box>
				<Box themeColor="#dd4814">ORANGE2</Box>
				<Box themeColor="hsl(15, 70%, 94%)">ORANGE_FACED</Box>
				<Box themeColor="pink">PINK</Box>
			</>
			<>
				<Box themeColor="yellow">YELLOW</Box>
				<Box themeColor="green">GREEN</Box>
				<Box themeColor="#3ebf6a">GREEN2</Box>
				<Box themeColor="#267340">GREEN_DARK</Box>
				<Box themeColor="rgba(62, 191, 106, .1)">GREEN_FACED</Box>
			</>
		  <>
				<Box themeColor="CYAN"></Box>
				<Box themeColor="teal">TEAL</Box>
			</>
			<>
				<Box themeColor="BLUE">BLUE</Box>
				<Box themeColor="rgb(26,115, 232)">BLUE_600</Box>
				<Box themeColor="rgb(25,103, 210)">BLUE_600</Box>
			</>
			<>
				<Box themeColor="GRAY">GRAY</Box>
				<Box themeColor="rgb(248, 249, 250)">GRAY_50</Box>
				<Box themeColor="rgb(218, 220, 224)">GRAY_300</Box>
				<Box themeColor="rgb(154, 160, 166)">GRAY_500</Box>
				<Box themeColor="rgb(128, 134, 139)">GRAY_600</Box>
				<Box themeColor="rgb(95, 99, 104)">GRAY_700</Box>
				<Box themeColor="#f9f9f9">GRAY_EXTRALIGHT</Box>
				<Box themeColor="#d3d3d3">GRAY_LIGHT</Box>
				<Box themeColor="#575759">DARK_GRAY</Box>
			</>
			</>
);



export default Colors;
