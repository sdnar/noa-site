import React from 'react';

import PropTypes from 'prop-types';
import styled from 'styled-components';

import { MEDIA_SERVICES } from '../NoaMedia';


import { JsonView } from '../styles';
import { LayoutLite } from '../global/Layout';

import AboutScreen from  '../components/screens/AboutScreen/v3';
import TeamScreen from '../components/screens/TeamScreen';


export const AboutPage = () => (
<LayoutLite>
  <AboutScreen/>
  <TeamScreen/>
</LayoutLite>
);

export default AboutPage;
