import React from 'react';
import { useEffect, useState } from 'react';
import { useRef } from 'react'

import styled from 'styled-components';

import { css } from '@emotion/core';

import { LayoutLite } from '../global/Layout';

import {Line, Divider, Debug  } from '../styles';

import Noa from '../Noa';
import NoaSite from '../NoaSite';

import Hero from '../components/Hero';
import { NoHero } from '../components/Hero';
import HeroCarousel from '../components/HeroCarousel';

import ListOfServicesWidget  from '../fragments/ListOfServicesWidget';
import ListOfTecnologiesWidget  from '../fragments/TecnologiesList';
import ListOfClientsWidget from '../fragments/ListOfClientsWidget';

import WeAreCarouselWidget from '../fragments/WeAreCarousel';
import AboutUSWidget from '../fragments/AboutUS';
import WhoIsSIUWidget from '../fragments/WhoIsSIU';
import SIUPhraseWidget from '../fragments/SIUPhrase';

import WeHelpYouWidget from '../fragments/WeHelpYouWidget';

const _consultoriaPage = NoaSite.pages.consultoria;
const _wheAreSection = _consultoriaPage.sections.wheAreSection;
const _siuConsultantsSection = _consultoriaPage.sections.siuConsultantsSection;
const _siuServices = _consultoriaPage.sections.siuServicesSection;
const _whoIsSIU = _consultoriaPage.sections.whoIsSiu;
const _aboutUS = _consultoriaPage.sections.aboutUS;
const _wehelp = _consultoriaPage.sections.wehelpSection;

const _clients = _consultoriaPage.sections.clientsSection;


const ConsultoriaPage = (props) => {
return (
<LayoutLite>

<WeAreCarouselWidget slides={_wheAreSection.slides} /> <Line/>
    
<SIUPhraseWidget phrase={_siuConsultantsSection.phrases[0]}/> <Divider/>

<ListOfServicesWidget 
     title={_siuServices.title}
     subtitle={_siuServices.subtitle}
     legend={_siuServices.legend}
     services={_siuServices.services}
     css={_siuServices.css}
/> <Line/>

<WhoIsSIUWidget   css={_whoIsSIU.css}
    title={_whoIsSIU.title}
    subtitle={_whoIsSIU.subtitle}
    othertitle={_whoIsSIU.othertitle}
    products={_whoIsSIU.products} /> <Divider/>

<AboutUSWidget
    css={_aboutUS.css}
    title={_aboutUS.title}
    overview={_aboutUS.overview}
    owner={_aboutUS.owner}
    services={_aboutUS.services} /> 


     <div style={{alignItems: "center", textAlign: "center" } }  
	     class="column hero is-height  is-fullwidth ">
     <WeHelpYouWidget 
	     class="is-height"
	     title={_wehelp.title}
	     subtitle={_wehelp.subtitle}
	     legend={_wehelp.legend}
	     css={_wehelp.css}
	     accBtn={_wehelp.action}
     /> </div> <Divider/>

    <div style={{paddingLeft: "15%", paddingRight: "15%"}}>
     <ListOfClientsWidget
	     title={_clients.title}
	     subtitle={_clients.subtitle}
	     legend={_clients.legend}
	     clients={_clients.clients}
     />
     </div>


</LayoutLite>
);
}

export default ConsultoriaPage;
