module.exports = {
  siteMetadata: {
     title: 'Gatsby Storybook Starter',
     team: {
	sergio:	{ name: "Ing. Sergio Paz", role: "CEO", bio: "Socio fundador de NOA y nuestor experto en desarrollo y optimizacion de aplicaciones bancarias.", bio: "Sergio es socio fundador y manager de NOA. Tieme mas de 25 anos de experienca en la industria. Con frecuencia es convocado por sus conocimientos en auditoria y seguridad. Entre sus tareas a cargo de figuran el disenio y la ejecucion del plan de expansion de NOA en el segmento PyMe y Bancario."},
       mario:	{ name: "Ing. Mario Moreno", role: "CTO", overview: "Profesor de Informatica en UCEMA, especialista en arquitecturas y seguriad.",  bio: "Mario es uno de los socios fundadores y actual CTO de NOA. Apasiondad por la informatica y de los servidores, sus inquitodes no se limintan a la tecnologia sino tambien a la educacion y a la guitarra."},
	sandra: { name: "Ing. Sandra Geibelina", role: "COO", bio: "Sandra se desempena como Directora de Operaciones en NOA. Durante varios anios fue referente en argentina en los Sistemas Educativos SIU. Anteriormente se desempeno como Directora de Servicios Profesioneas en Educacion."},
	vero: { name: "Ing. Vero Eloe", role: "CMO", bio: "Veronica ademas de desempenarse como Directora de Marketing en NOA, es la Directora de Projectos y Servicios Bancarios. Apasionada por la informatica y los numeros, entre sus  tareas figuran el plan de expansion para noa en el segmento PyMe y Bancario."},
	 jose: { name: "Ing. Jose Pomares", role: "Gerente de Desarrollo",  bio: "Jose comenzo en NOA como programador.  Hoy como jefe de desarrollo en NOA, tiene la responsabilidad de coordinar la aplicacion del equipo y planificar el dessarrollo o adquisision de sistemas; supervisar el desarrolloy y las pruebas de los sistemas de aplicacion."},
	 count: "20",
	 tribu: [
	 "leidi", "cele", "pato", "romina", "javi", "diego", "javi", "gonza", "juan", "carlo", "igor", "simio" , "juan2", "vero" 
	 ]

     }
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'Gatsby Storybook Starter',
        short_name: 'Gatsby Storybook Starter',
        start_url: '/',
        background_color: '#ffffff',
        theme_color: '#744C9E',
        display: 'standalone',
        icon: 'src/assets/logos/icon.png',
      },
		}, 
		{
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/`,
        name: 'src',
      },
    },
		{
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/images`,
        name: 'images',
      },
    },

    'gatsby-plugin-root-import',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-offline',
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    'gatsby-plugin-styled-components',
    'gatsby-plugin-sass',
    'gatsby-background-image',
  ],
};
