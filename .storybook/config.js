import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { action } from '@storybook/addon-actions';
// import '@storybook/addon-console';

import styled from 'styled-components';
// import GlobalStyle from '../src/global/GlobalStyle';
//  <GlobalStyle />

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;
/*
  <Wrapper>
  </Wrapper>
  */

// const Decorator = storyFn => (
//     {storyFn()}
// );
// 
// addDecorator(Decorator);

global.___loader = {
  enqueue: () => { },
  hovering: () => { },
};

global.__PATH_PREFIX__ = '';
window.___navigate = (pathname) => {
  action('NavigateTo:')(pathname);
};

// const req = require.context('../src', true, /\.stories\.js$/);
const req = require.context('../stories', true, /\.stories\.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);

configure(require.context('../stories', true, /\.stories\.js$/), module);
