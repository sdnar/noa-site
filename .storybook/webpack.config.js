const path = require("path");

module.exports = ({ config }) => {
  config.module.rules[0].exclude = [/node_modules\/(?!(gatsby)\/)/];

  config.module.rules[0].use[0].loader = require.resolve("babel-loader");

  config.module.rules[0].use[0].options.presets = [
    require.resolve("@babel/preset-react"),
    require.resolve("@babel/preset-env"),
  ];

  config.module.rules[0].use[0].options.plugins = [
    require.resolve("@babel/plugin-proposal-class-properties"),
    require.resolve("babel-plugin-remove-graphql-queries"),
  ];

   config.module.rules.push(
     {
    test: /\.(scss)$/,
    use: [
      "style-loader",
      "css-loader",
      {
        loader: "sass-loader",
        options: {
          data: '@import "./global.scss";',
         includePaths: [__dirname, "./src/assets/scss/"]
        }
      }
    ]
   });

//   config.module.rules.push(
//   {
//         test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
//         loaders: ["file-loader"]
//   });


  // Prefer Gatsby ES6 entrypoint (module) over commonjs (main) entrypoint
  config.resolve.mainFields = ["browser", "module", "main"];

  delete config.resolve.alias['core-js'];
  return config
};

// module.exports = (baseConfig, env, defaultConfig) => {
// 
//     config.module = {
// 	 rules: [
//       {
//         test: /\.(css|scss)$/,
//         use: [
//           "style-loader",
//           "css-loader",
//           {
//             loader: "sass-loader",
//             options: {
//               data: '@import "./global.scss";',
// 	      includePaths: [__dirname, "./src/assets/scss/"]
//             }
//           }
//         ]
//       },
//       {
//         test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
//         loaders: ["file-loader"]
//       }
// ]
//     }
// };
